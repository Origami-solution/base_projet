<?php

	$this->set_css($this->default_theme_path.'/prospami/css/prospami.css');
	$this->set_js_lib($this->default_theme_path.'/prospami/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/prospami/js/flexigrid-add.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div class="mDiv">
		<div class="ftitle">
			<div class='ftitle-left'>
				<?php echo $this->l('form_add'); ?> <?php echo $subject?>
			</div>
			<div class='clear'></div>
		</div>
		<div title="<?php echo $this->l('minimize_maximize');?>" class="ptogtitle">
			<span></span>
		</div>
	</div>
<div id='main-table-box'>
	<?php echo form_open( $insert_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
		<div class='form-div'>
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
					<?php echo $input_fields[$field->field_name]->input?>
				</div>
				<div class='clear'></div>
			</div>
			<?php }?>
			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>

			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div>
		<div class="pDiv">
			<div class='form-button-box'>
				<input id="form-button-save" type='submit' value='<?php echo $this->l('form_save'); ?>'  class="btn btn-primary"/>
			</div>
<?php 	if(!$this->unset_back_to_list) { ?>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_save_and_go_back'); ?>' id="save-and-go-back-button"  class="btn btn-success"/>
			</div>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_cancel'); ?>' class="btn btn-default" id="cancel-button" />
			</div>
<?php 	} ?>
			<div class='form-button-box'>
				<div class='small-loading' id='FormLoading'><?php echo $this->l('form_insert_loading'); ?></div>
			</div>
			<div class='clear'></div>
		</div>
	<?php echo form_close(); ?>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
	

	var url_post_nouvelle_selection = '<?php echo site_url("mediatheque/ajax_url_post_nouvelle_selection") ;?>';
	var url_ajax_liste_des_images_preselect = '<?php echo site_url("mediatheque/ajax_liste_des_images_preselect"); ?>';




</script>



<script type="text/javascript">
	
$(document).ready(function() {



			$("#various3").fancybox({
				'width'				: '95%',
				'height'			: '95%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});





$("#various3").click(function(){

var form_data = {

liste : $("#liste_photos").val(),
ajax : '1'
};

			$.ajax({
					              url: url_post_nouvelle_selection,
					              type: 'POST',
					              async : false,
					              data: form_data,

					    success: function(msg) {
					//alert(msg);

									          if(msg == "OK"){

									          }else{        


									        		}
						},
						complete: function(){

						}

				});

});







var elements_actuel = $('#liste_photos').val();

$('#liste_des_photos_selectionnes').load(url_ajax_liste_des_images_preselect + "/" + encodeURIComponent(elements_actuel));








$(".icons_selectionne").click(function(){

//var id_commentaire = $(this).attr('id');
var url = $(this).data("path");

//on ajoute la valeur au champs
$('#icon_selection').val(url);
$('#val_icon_selection').html("<img src=\"" + url + "\"  />");
     //   alert(url);

$('#modal_icons').modal('hide');

    });





});








</script>