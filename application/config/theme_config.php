<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| TEMPLATE
|--------------------------------------------------------------------------
|
| Permet de choisir le template du site.
|
*/

$config['theme']			= "yeti"; 
$config['theme_admin']		= "admin"; 