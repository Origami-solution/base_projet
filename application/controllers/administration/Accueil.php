<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***************************
Gilles GUIGON - winprod@sfr.fr
18/07/2013

****************************/

class Accueil extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
	    		

	    		$this->load->library("admin_lib");
	    		$this->output->enable_profiler(TRUE);
	}


	public function config_admin()
	{


		$data["liste_acces"] = $this->admin_lib->liste_acces(); //liste du menu de gauche


		$data["titre"] 			= "Tableau de bord"; 
		$data["menu_titre"] 	= "Tableau de bord"; 
		$data["well_page"] 		= "";
	
		$data["nav_selected"] 		= "Tableau de bord";

		return $data;


	}






	public function index()
	{


		$data = $this->config_admin();





		//$this->load->model("config_model");

		//$data["liste_des_demandes_activation"] = $this->users_model->get_liste_demandes_activation();
		//$data["statut_du_site"] = $this->config_model->get_statut_site();
	


#statistiques
	/*	$this->load->model("stats_model");

		$data["stat_nbr_visiteurs_today"] = $this->stats_model->count_nbr_visiteurs(time(),time() - 86400);
		$data["stat_nbr_visiteurs_yesterday"] = $this->stats_model->count_nbr_visiteurs(time() - 86400,time() - (86400 * 2));
		$data["stat_nbr_visiteurs_total"] = $this->stats_model->count_nbr_visiteurs(time(),1390234905);
		$data["stat_nbr_visiteurs_j30"] = $this->stats_model->count_nbr_visiteurs(time(),time() - (86400 * 30));

		$data["stat_nbr_pages_today"] = $this->stats_model->count_nbr_page(time(),time() - 86400);
		$data["stat_nbr_pages_yesterday"] = $this->stats_model->count_nbr_page(time() - 86400,time() - (86400 * 2));
		$data["stat_nbr_pages_total"] = $this->stats_model->count_nbr_page(time(),1390234905);
		$data["stat_nbr_pages_j30"] = $this->stats_model->count_nbr_page(time(),time() - (86400 * 30));


*/

		

		$data["contenu"] = theme_admin()."/v_admin_accueil";
		$this->load->view(theme_admin()."/template.tpl.php",$data);
	
	
	}








}