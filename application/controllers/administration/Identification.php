<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***************************
Gilles GUIGON - winprod@sfr.fr
18/07/2013

****************************/

class Identification extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		

	}



	public function index()
	{
		
		if(verif_admin() == TRUE){	redirect("administration/accueil");  }


	$this->load->library('form_validation');

	$this->form_validation->set_rules('identification_email', "Email", 'trim|required');
	$this->form_validation->set_rules('identification_pass', "Mot de passe", 'trim|required');

		if($this->form_validation->run() == TRUE) {


			#on verif si ok 
			$this->load->helper('cryptage');
			$encrypted_string = encrypt($this->input->post("identification_pass"));

			
			$infos_user = $this->users_model->get_login($this->input->post("identification_email"),$encrypted_string);



				if(!empty($infos_user)){

					foreach ($infos_user as $key => $value) {}

						

						#mise en session de l'utilisateur

									$newdata = array(
												'user_id'  => $value -> id,
												'login' => true,


												'user_avatar'  	=> $value -> avatar,
												'user_prenom'  	=> $value -> prenom,
												'user_nom'  	=> $value -> nom,
												'user_pseudo'  	=> $value -> pseudo,
												'user_email'  	=> $value -> email,

												'user_compte_type'  	=> $value -> compte_type,

												);

									$this->session->set_userdata($newdata);

						#mise a jour de la connexion de l'utilisateur (stats)


									$data_user = array(
											            'date_last_connexion' => date("Y-m-d H:i:s")
											            );
									
									$this->users_model->update($data_user, $value -> id);
								
							redirect("administration/accueil");

						}


		}


		$data["contenu"] = theme_admin()."/login/v_login";
		$this->load->view(theme_admin()."/login/identification.tpl.php",$data);
		
	
	}






	public function recup_password()
	{
		
		//permet de retourner le mot de passe perdu


#si deja co on redirect
		if(verif_admin() == TRUE){
	    			redirect("administration/accueil");
	    }

			$this->load->library('form_validation');


			$this->form_validation->set_rules('identification_email', "Email", 'trim|required');
			
		if($this->form_validation->run() == TRUE) {

			$infos_user = $this->users_model->verification_mail_existe($this->input->post("identification_email"));


				if(!empty($infos_user)){

					foreach ($infos_user as $key => $value) {}

/**
 * Envoi de l'email
 */
					

					$this->emails_lib->Mail__Recup_mot_de_passe($infos_user);




					$this->session->set_flashdata('message_ok', 'Vous allez recevoir un email avec votre mot de passse.');

					redirect("administration/identification");


				}else{

					$this->session->set_flashdata('message_error', 'Email inconnu');
				}


		}


		
		$data["contenu"] = theme_admin()."/login/v_login_perdu";
		$this->load->view(theme_admin()."/login/identification.tpl.php",$data);
		
	
	}
















	## Lien de déconnexion
	public function deconnexion()
	{

		//$this->output->enable_profiler(TRUE);


	$newdata = array(
								'user_id'  => "",
								'login'     => false,
								
												'user_avatar'  	=> "",
												'user_prenom'  	=> "",
												'user_nom'  	=> "",
												'user_pseudo'  	=> "",
												'user_email'  	=> "",
												'user_compte_type'  	=> "",

								);

	$this->session->set_userdata($newdata);
	redirect("administration/identification");
	}





}