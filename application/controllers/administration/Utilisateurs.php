<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Utilisateurs extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		
		

			$this->load->library("admin_lib");
	
	}




	public function config_admin()
	{


		$data["liste_acces"] = $this->admin_lib->liste_acces(); //liste du menu de gauche


		$data["titre"] 			= "Les utilisateurs"; 
		$data["menu_titre"] 	= ""; 
		$data["well_page"] 		= "";
	
		$data["nav_selected"] 		= "Utilisateurs";

		return $data;


	}




public function index()
	{
		
		$data = $this->config_admin();




		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('prospami');
			$crud->set_table('users');
			$crud->set_subject('un utilisateur');


			if(typecompte() == true){



			}else{
				$crud->where('users.id',$this->session->userdata('user_id'));
				//$crud->unset_update);
			}


			$crud->set_field_upload('avatar','assets/uploads/avatars');

			$crud->columns('civilite','prenom','nom','email','prenom','pseudo','etat');
	


	$state = $crud->getState();
    $state_info = $crud->getStateInfo();
 
    if($state == 'add')
    {
        $crud->fields('civilite', 'prenom', 'nom' , 'pseudo', 'email' ,'avatar', 'date_inscription' ,'compte_type','adresse','code_postal','ville','pays','tel_fixe','tel_portable','pass');


			//$crud->change_field_type('statut','invisible');

        	$crud->field_type('compte_type','enum',array('Administrateur','Gestionnaire','Comptabilité','Modérateur','Utilisateur'));

			$crud->change_field_type('messagerie_alert_mail','invisible');
			$crud->change_field_type('date_inscription','invisible');






    }
    elseif($state == 'edit')
    {
        //$primary_key = $state_info->primary_key;
        //Do your awesome coding here. 
        


if(typecompte() == true){

$crud->fields('civilite', 'prenom', 'nom' , 'pseudo', 'email','avatar','compte_type','adresse','code_postal','ville','pays','tel_fixe','tel_portable');
$crud->required_fields('civilite','prenom', 'nom' , 'pseudo', 'email','compte_type');
}else{
	$crud->fields('civilite', 'prenom', 'nom' , 'pseudo', 'email','adresse','code_postal','ville','pays','tel_fixe','tel_portable');
	$crud->required_fields('civilite','prenom', 'nom' , 'pseudo', 'email');
}




    }else{

    }

    		



        	$crud->field_type('compte_type','enum',array('Administrateur','Gestionnaire','Comptabilité','Modérateur','Utilisateur'));

	 		$crud->callback_after_upload(array($this,'call_after_upload_avatar'));

	 		//$crud->callback_column('actions',array($this,'_callback_actions_generale'));
	 		$crud->callback_column('utilisateur',array($this,'_callback_utilisateur_generale'));
	 		$crud->callback_column('etat',array($this,'_callback_statut_generale'));

	 		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
	 	
			//$crud->display_as('','');
	 		$crud->callback_after_insert(array($this, 'utilisateur_after_insert'));


if(typecompte() == true){



}else{
	$crud->unset_delete();
	$crud->unset_add();
}

			$crud->unset_jquery();
			$crud->unset_read();
			//$crud->unset_export();
			$crud->unset_print();

			//$crud->unset_delete();
			//$crud->unset_jquery_ui();


			$data["tableau"]= $crud->render();
			

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}


		
		$data["contenu"] = theme_admin()."/utilisateurs/v_page_tableau";
		$this->load->view(theme_admin()."/template.tpl.php",$data);
	
	}


function utilisateur_after_insert($post_array,$primary_key)
{
   
  	$this->os_email->Envoi_mail_User(1,2);
 
    return true;
}






function encrypt_password_callback($post_array) {


	$this->load->helper('string');
	$this->load->helper('cryptage');

	if(!empty($post_array['pass'])){
		$mdp = $post_array['pass'];
		$post_array['pass'] = encrypt($post_array['pass']);

	}else{
		$mdp = random_string('alnum', 6);
		$post_array['pass'] = encrypt($mdp);
	}


 	$post_array['date_inscription'] = date('Y-m-d H:i:s');
	$post_array['hash'] = md5($post_array['email']);
	$post_array['messagerie_alert_mail'] = "Oui";
	$post_array['statut'] = "Actif";


	//$this->load->library('mailing');
	//$this->mailing->action_Add_Email_en_Newsletter($post_array['email']);
	//$this->os_users->action_Mail__Inscription($post_array['email'],$mdp);


  return $post_array;
}









function _callback_utilisateur_generale($value, $row)
{

return $row -> civilite ." ". $row -> prenom ." ". $row -> nom ."<br>". $row -> pseudo."<br>".  $row -> email;
}




function _callback_statut_generale($value, $row)
{

	if($row -> statut == "Actif"){

		return "<img src=\"".admin_image_url("icons/ok.png")."\" height=\"30px\"  >";

	}elseif($row -> statut == "Inactif"){

		return "<img src=\"".admin_image_url("icons/neutre.png")."\" height=\"30px\"  >";
	
	}elseif($row -> statut == "Bloquer"){

		return "<img src=\"".admin_image_url("icons/defender.png")."\" height=\"30px\"  >";
	
	}elseif($row -> statut == "Supprimer"){

		return "<img src=\"".admin_image_url("icons/delete.png")."\" height=\"30px\"  >";
	}



}






function call_after_upload_avatar($uploader_response,$field_info, $files_to_upload)
{
    $this->load->library('image_moo');
 
    //Is only one file uploaded so it ok to use it with $uploader_response[0].
    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
 #retaille la photo d'origine
$this->image_moo->load($file_uploaded)->resize(800,800)->save($file_uploaded,true);

$this->image_moo->load($file_uploaded)
	->resize_crop(100,100)->save($field_info->upload_path."/small/".$uploader_response[0]->name);

    return true;
}


}