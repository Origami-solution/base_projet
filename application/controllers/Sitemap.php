<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Sitemap extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }




/**
 * Permet de générer un fichier robot.txt si celui ci n'existe pas
 * @date   2015-05-26
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
    public function verif_fichier()
    {

        $filename = 'robots.txt';

        if (file_exists($filename)) {
           
            unlink($filename);


        }

        $monfichier = fopen($filename, 'a+');

        fseek($monfichier, 0); //curseur au point de départ

        fputs($monfichier, 'User-agent: *
Disallow: /assets/
Disallow: /system/
Allow: /assets/'.theme().'/images/
Sitemap: '.site_url("sitemap"));

        fclose($monfichier);

    }

        





/**
 * Permet la génération du sitemap
 * @date   2015-05-04
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
   public function index()
    {


             $this->verif_fichier();

/*
Liste des Controllers a référencés
*/
//echo "test new";

        $data["liste"][] = array( "titre" => "Accueil" , "url" => site_url(""), "priorite" => "1");
        $data["liste"][] = array( "titre" => "Contact" , "url" => site_url("contact"), "priorite" => "0.5");
        $data["liste"][] = array( "titre" => "Recherche" , "url" => site_url("recherche"), "priorite" => "0.5");
        $data["liste"][] = array( "titre" => "Phototheque" , "url" => site_url("phototheque"), "priorite" => "0.8");
        $data["liste"][] = array( "titre" => "Actualité" , "url" => site_url("blog"), "priorite" => "0.8");


//var_dump($data["liste"]);


/**
 * Ajout des URL provenant des pages
 * 
 */
 /*       
        $this->load->library("Os_pages");
        $data["liste_pages"] = $this->os_pages->liste_sitemap();

        if(!empty($data["liste_pages"])){

            foreach ($data["liste_pages"] as $key => $r) {

                $data["liste"][] = array( "titre" => $r ->  titre, "url" => site_url("pages/index/".$r ->  slug), "priorite" => "0.8");

            }
        }
*/

/**
 * Ajout des URL provenant du blog
 * 
 */
      /*  
        $this->load->library("Os_pages");
        $data["liste_pages"] = $this->os_pages->liste_sitemap();

        if(!empty($data["liste_pages"])){

            foreach ($data["liste_pages"] as $key => $r) {

                $data["liste"][] = array( "titre" => $r ->  titre, "url" => site_url("pages/index/".$r ->  slug), "priorite" => "0.8");

            }
        }
*/



header("Content-Type: text/xml;charset=iso-8859-1");
$this->load->view("tools/v_sitemap_default",$data);
    
    }




}
