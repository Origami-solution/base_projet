<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {





	public function __construct()
	{
		parent::__construct();
	    		



	}


	/**
	 * Les métats de la page
	 * @date   2015-05-26
	 * @auteur Gilles     Guigon        - contact@origami-solution.com
	 * @return [type]     [description]
	 */
	public function metats()
	{

			#recup des metats du fichier de trad ou du fichier admin
			$data['titre_site'] = lang("titre_site");
			$data['description_site'] 	= lang("description_site");
			$data['mots_cles_site'] 	= lang("mots_cles_site");
			/*
			$data["og_sociaux"] = array(

				'og:site_name' => lang("titre_site"),
				'og:title' => lang("titre_page"),
				'og:description' => lang("description_site"),
				'og:type' => "website", //website, article, video, music...
				'og:locale' => "fr_FR", //en_US
				'og:url' => site_url(),
				'og:image' => base_url().$this->config->item("theme").'/images/logo.png',

			);
			*/
			return $data ;

	}



	public function index()
	{
		
	$data = $this->config_lib->infos_generales();
	$data = array_merge($data,$this->metats());
	
	$data["liste_mails"] = $this->emails_model->get();//test de array
	

	$data["menu_top"][3]["class"] = "active"; //menu actif


	$data["contenu"] = theme().'/menu.twig';

	echo $this->twig->render(theme().'/template.twig',$data);


	}
}
