<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

	





/**
 * Controle si l'utilisateur est administrateur ou non, 
 * Si il peux accèder a l'administration
 * Retourne le type de compte
 * @date   2015-05-15
 * @auteur Gilles     Guigon -             contact@origami-solution.com
 * @param  [type]     $id    [description]
 * @return [type]            [description]
 */
function get_info_compte_admin($id) 
     {
       
          $this->db->where('id',$id);
          $this->db->where('statut',"Actif");
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data = $row -> compte_type;
      }
      return $data;
        }
}
















/**
 * EX PARTIE A VERIFIER
 */


#Vérification si le compte existe, si le mail est dans la base de donnée
function verification_mail_existe($email) 
     {
       
          $this->db->where('email',$email);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}





#identification verification si le login et le mot de passe sont associés
function get_login($email,$mdp) 
     {
       
      
          $this->db->where('email',$email);
          $this->db->where('pass',$mdp);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}


function update($data, $id)
     {
        $this->db->where('id', $id);
        $this->db->update('users',$data);
    }



}

/* End of file os_users_model.php */
/* Location: ./application/models/os_users_model.php */