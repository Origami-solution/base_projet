<?php
class Users_model extends CI_Model {


#Vérification si le compte existe, si le mail est dans la base de donnée
function verification_mail_existe($email) 
     {
       
          $this->db->where('email',$email);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}


#Vérification si le compte existe, si le mail est dans la base de donnée
function verification_mail_existe_all($email) 
     {
       
          $this->db->where('email',$email);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 return true;
            }
}




function insert($data)
     {
         $this->db->insert('users', $data);
     }



function update($data, $id)
     {
        $this->db->where('id', $id);
        $this->db->update('users',$data);
    }


function get_login($login,$mdp) 
     {
       
      
          $this->db->where('email',$login);
          $this->db->where('pass',$mdp);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
    }
}















#récup des infos du compte
function get_info_compte($id) 
     {
       
          $this->db->where('id',$id);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}


#récup du type  du compte
function get_type_compte($id) 
     {
       
          $this->db->where('id',$id);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data = $row -> compte_type;
      }
      return $data;
        }
}


#récup des infos du compte admin
function get_info_compte_admin($id) 
     {
       
          $this->db->where('id',$id);
          $this->db->where('statut',"Actif");
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data = $row -> compte_type;
      }
      return $data;
        }
}

#récup la liste des clients
function get_liste_users_all($id) 
     {
           $this->db->select('users.id,users.prenom,users.nom,users.pseudo');


          $this->db->where('id !=',$id);
          $this->db->where('statut !=',"bloquer");
          $this->db->order_by('users.prenom',"ASC");

          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}

#récup des infos du compte
function get_liste_users($id) 
     {
           $this->db->select('users.id,users.prenom,users.nom,users.pseudo');


          $this->db->where('id !=',$id);
          $this->db->where('statut !=',"bloquer");

          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}




#récup des infos du compte depuis l'hash
function recup_user_hash($hash) 
     {
       
          $this->db->where('hash',$hash);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}




#identification verification si le login existe
function get_compte($email) 
     {
       
      
          $this->db->where('email',$email);
          $query = $this->db->get('users');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}









######SUPPRESSION DU COMPTE

function add_procedure_suppression($data)
     {
         $this->db->insert('users_suppression_compte', $data);
     }

#récup si le compte est en cours de suppression
function get_info_compte_suppression($id_user) 
     {
       
          $this->db->where('id_user',$id_user);
          $query = $this->db->get('users_suppression_compte');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}



function delete_procedure_suppression($id_user)
     {
         $this->db->where('id_user',$id_user);
         $this->db->delete('users_suppression_compte');
     }








######ACTIVATION



function insert_demande_activation($data)
     {
         $this->db->insert('users_demande_reactivation', $data);
     }




#récup liste des activations
function get_liste_demandes_activation() 
     {
       
          $this->db->where('date_de_traitement',NULL);
          $this->db->group_by('id_user');
          $query = $this->db->get('users_demande_reactivation');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}

#récup des infos
function get_infos_demandes_activation($id_demande,$id_user) 
     {
       
          $this->db->where('id',$id_demande);
          $this->db->where('id_user',$id_user);
          
          $query = $this->db->get('users_demande_reactivation');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}


#verifie si demande deja existante
function demande_existante($id_user) 
     {
       
          $this->db->where('id_user',$id_user);
          $this->db->where('statut',"En attente");
          $query = $this->db->get('users_demande_reactivation');
          
         if($query->num_rows()>0)
             {
             
              return true;
            }
}

     



function update_demande_activation($data, $id)
     {
        $this->db->where('id', $id);
        $this->db->update('users_demande_reactivation',$data);
    }


     
/*

function get_login($login,$mdp) 
     {
       
      
          $this->db->where('login',$login);
          $this->db->where('pass',$mdp);
          $query = $this->db->get('contact');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
    }
}

function get_id($id) 
     {
       
      
          $this->db->where('id',$id);
          $query = $this->db->get('contact');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
    }
}
*/

}