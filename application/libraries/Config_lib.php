<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CONFIGURATION GENERALE
 */


class Config_lib
{
  protected 	$ci;

	public function __construct()
	{
		//$this->__verif_acces();
	}







	/**
	 * GESTION GENERALE DU TEMPLATE POUR TWIG
	 * @date   2015-05-15
	 * @auteur Gilles     Guigon        - contact@origami-solution.com
	 * @return [type]     [description]
	 */
	public function infos_generales()
		{
	  		$CI =& get_instance(); 

		
		$data['titre_site'] = lang("titre_site");
		$data['titre_page'] = "";


		$data['site_url'] = site_url();


		$data['description_site'] 	= lang("description_site");
		$data['mots_cles_site'] 	= lang("mots_cles_site");
		$data['auteur_site'] 		= lang("auteur_site");
		
		#Partie OG:SOCIAUX par defaut
		$data["og_sociaux"] = array(

			'og:site_name' => lang("titre_site"),
			'og:title' => lang("titre_page"),
			'og:description' => lang("description_site"),
			'og:type' => "website", //website, article, video, music...
			'og:locale' => "fr_FR", //en_US
			'og:url' => site_url(),
			'og:image' => base_url().'/assets/'.$CI->config->item("theme").'/images/logo.png',



			);
		
		#favicon
		$data["favicon"] = base_url().'/assets/'.$CI->config->item("theme").'/images/favicon.ico';			
			
		#information du template
		$data["template"] = $CI->config->item("theme");
		$data["template_header"] = $CI->config->item("theme")."/header.twig";
		$data["template_footer"] = $CI->config->item("theme")."/footer.twig";




		$data["menu"] = array(

		1 => array('titre' => 'test 1', 'url' => site_url()),
		2 => array('titre' => 'test 2', 'url' => site_url()),
		3 => array('titre' => 'test 3', 'url' => site_url()),
		4 => array('titre' => 'test 4', 'url' => site_url()),
		5 => array('titre' => 'test 5', 'url' => site_url())
		);


		$data["menu_top"] = array(

		1 => array('titre' => 'Accueil', 'url' => site_url(), 'class' => ''),
		2 => array('titre' => 'Photos', 'url' => site_url(), 'class' => ''),
		3 => array('titre' => 'Images', 'url' => site_url(), 'class' => '',

			array('titre' => 'Imagesdfs', 'url' => site_url(), 'class' => ''),
			array('titre' => 'Imagessdfsdfsds', 'url' => site_url(), 'class' => ''),
			array('titre' => 'Imagesdqsdfqs', 'url' => site_url(), 'class' => ''),

			),


		4 => array('titre' => 'Tourisme', 'url' => site_url(), 'class' => ''),
		5 => array('titre' => 'Valeur', 'url' => site_url(), 'class' => '')
		);

		/**
		 * Fichers CSS
		 */
		$data["liste_css"][]["url"] = css_url("bootstrap.min.css");
		$data["liste_css"][]["url"] = css_url("font-awesome.min.css");
	
		/**
		 * Fichers JS
		 */
		$data["liste_js"][]["url"] 	= js_url("jquery.min.js");
		$data["liste_js"][]["url"] 	= js_url("bootstrap.min.js");
		//$data["liste_js"][]["url"] 	= js_url("masonry.pkgd.min.js");



	  		return $data;



		}






}

/* End of file OS_users.php */
/* Location: ./application/libraries/OS_users.php */
