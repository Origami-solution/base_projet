<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 	GESTION DES UTILISATEURS DU SITE
 *  
 *
 *
 * @package		CodeIgniter 2.2.1
 * @author 		Guigon Gilles contact@origami-solution.com
 * @version     1.0.0
 *
 **/



class Admin_lib
{
  protected 	$ci;

	public function __construct()
	{
		$this->__verif_acces();
	}







	/**
	 * Liste des accès du menu de l'admin
	 * @date   2015-05-15
	 * @auteur Gilles     Guigon        - contact@origami-solution.com
	 * @return [type]     [description]
	 */
	public function liste_acces()
		{
	  		$CI =& get_instance(); 


	  		$data["tableau_de_bord"] = true;
	  		$data["utilisateurs"] = true;



	  		return $data;



		}













	public function __verif_acces()
		{
				$CI =& get_instance(); 

				if(verif_admin() == FALSE){	redirect("administration/identification");	}
				$CI->load->library('grocery_CRUD');
		}

}

/* End of file OS_users.php */
/* Location: ./application/libraries/OS_users.php */
