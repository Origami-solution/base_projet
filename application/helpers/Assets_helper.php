<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');




      if ( ! function_exists('image_url'))
      {
        function image_url($nom)
        {
          return base_url() .'assets/'.theme().'/images/'. $nom;
        }
      }



      if ( ! function_exists('css_url'))
      {
        function css_url($nom)
        {
          return base_url() . 'assets/'.theme().'/css/' . $nom;
        }
      }

      if ( ! function_exists('css_min_url'))
      {
        function css_min_url($nom)
        {
          return base_url() . 'assets/'.theme().'/min/' . $nom;
        }
      }


      if ( ! function_exists('js_url'))
      {
        function js_url($nom)
        {
          return base_url() . 'assets/'.theme().'/js/' . $nom;
        }
      }



/* PARTIE ADMIN */

      if ( ! function_exists('admin_image_url'))
      {
        function admin_image_url($nom)
        {
          return base_url() .'assets/'.theme_admin().'/img/'. $nom;
        }
      }


      if ( ! function_exists('admin_css_url'))
      {
        function admin_css_url($nom)
        {
          return base_url() . 'assets/'.theme_admin().'/css/' . $nom;
        }
      }



      if ( ! function_exists('admin_js_url'))
      {
        function admin_js_url($nom)
        {
          return base_url() . 'assets/'.theme_admin().'/js/' . $nom;
        }
      }

      if ( ! function_exists('admin_plugins_url'))
      {
        function admin_plugins_url($nom)
        {
          return base_url() . 'assets/'.theme_admin().'/plugins/' . $nom;
        }
      }
