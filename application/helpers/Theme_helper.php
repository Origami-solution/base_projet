<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Les themes
 * @date   2015-05-15
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
function theme()
{
$CI =& get_instance();

  return $CI->config->item('theme');
}


function theme_admin()
{
$CI =& get_instance();

  return $CI->config->item('theme_admin');
}
