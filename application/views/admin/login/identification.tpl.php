<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

    <meta charset="utf-8">
    <title>Administration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<?php echo admin_css_url("icons/icons.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("plugins.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("style_admin.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("animate-custom.css"); ?>" rel="stylesheet">


    <script src="<?php echo admin_plugins_url("modernizr/modernizr-2.6.2-respond-1.1.0.min.js"); ?>"></script>
</head>
<body>
<?php $this->load->view($contenu); ?>

    <script src="<?php echo admin_plugins_url("jquery-2.1.1.min.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("jquery-ui/jquery-ui-1.10.4.min.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("bootstrap/bootstrap.min.js"); ?>"></script>

    <script src="<?php echo admin_plugins_url("backstretch/backstretch.min.js"); ?>"></script>
    <script src="<?php echo admin_js_url("account.js"); ?>"></script>

</body>

</html>
