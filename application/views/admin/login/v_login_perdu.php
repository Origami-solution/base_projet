<div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="login-logo">
                        
                    </div>
                   
                    <div class="login-form">
                        <!-- BEGIN ERROR BOX -->
                      

                        <?php if(validation_errors() == true){ ?>

                       <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                     	<?php echo validation_errors(); ?>                  
                        </div>

						<?php } ?>
                        <?php 
                       $message_error = $this->session->flashdata('message_error');
                       $message_ok = $this->session->flashdata('message_ok');

                        if($message_error == true){ ?>

                       <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                        <?php echo $message_error; ?>                  
                        </div>

                        <?php } ?>
                        <?php

                        

                        if(!empty($message_ok)){ ?>

                       <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                        <?php echo $message_ok; ?>                  
                        </div>

                        <?php }  ?>



                        <!-- END ERROR BOX -->
                        <form action="<?php echo site_url("administration/identification/recup_password"); ?>" method="post">
                            <input type="text" placeholder="Email" name="identification_email" class="input-field form-control user" value="<?php echo set_value('identification_email'); ?>" />
                            
                            <button type="submit" class="btn btn-login">Récupérer mon mot de passe</button>
                        </form>
                       <div class="login-links">
                            <a href="<?php echo site_url("administration/identification"); ?>">Se connecter</a>                    
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>