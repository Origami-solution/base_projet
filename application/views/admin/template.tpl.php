<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

    <meta charset="utf-8">
    <title>Administration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="<?php echo admin_css_url("icons/icons.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("plugins.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_css_url("style_admin.css"); ?>" rel="stylesheet">
    <link href="<?php echo admin_plugins_url("font-awesome-4.1.0/css/font-awesome.css"); ?>" rel="stylesheet">

   
    <script src="<?php echo admin_plugins_url("modernizr/modernizr-2.6.2-respond-1.1.0.min.js"); ?>"></script>


    <!-- BEGIN MANDATORY SCRIPTS-->

    <script src="<?php echo admin_plugins_url("jquery-1.11.js"); ?>"></script>

    <script src="<?php echo admin_plugins_url("jquery-migrate-1.2.1.js"); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js"></script>


<?php     if(!empty($tableau)){
   foreach($tableau AS $cle => $valeur) 
     { 
           
             if($cle == "output"){}
             elseif($cle == "js_files"){
               
             foreach($valeur AS $cle_s => $valeur_s) 
                {
        
     
        ?>
        <script src="<?php echo $valeur_s; ?>"></script>
        <?php  
        

                }
            }
            elseif($cle == "css_files"){
             
             foreach($valeur AS $cle_r => $valeur_r) 
                {
        
       
        ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $valeur_r; ?>" />
    
        <?php  
                }
            } 
     } 
} ?>



    <script src="<?php echo admin_plugins_url("bootstrap/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("bootstrap-dropdown/bootstrap-hover-dropdown.min.js"); ?>"></script>
    <!--<script src="<?php echo base_url(); ?>assets_admin/plugins/bootstrap-select/bootstrap-select.js"></script>-->
    <script src="<?php echo admin_plugins_url("icheck/icheck.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("mmenu/js/jquery.mmenu.min.all.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("nprogress/nprogress.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("charts-sparkline/sparkline.min.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("breakpoints/breakpoints.js"); ?>"></script>
    <script src="<?php echo admin_plugins_url("numerator/jquery-numerator.js"); ?>"></script>


</head>

<body>
  
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        
<?php $this->load->view(theme_admin()."/menu_top"); ?>
<?php $this->load->view(theme_admin()."/menu_gauche"); ?>
<?php $this->load->view($contenu); ?>


    </div>
    <script src="<?php echo admin_js_url("application.js"); ?>"></script>


</body>
</html>