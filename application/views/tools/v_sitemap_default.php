<?php header('Content-type: text/xml'); ?>
<?= '<?xml version="1.0" encoding="UTF-8" ?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo site_url(); ?></loc> 
        <priority>1.0</priority>
    </url>

    


    <?php if(!empty($liste)){
        foreach ($liste as $key => $r) {
    
     ?>
        <url>
            <loc><?php echo $r["url"]; ?></loc>
            <changefreq>monthly</changefreq>
            <priority><?php echo $r["priorite"]; ?></priority>
        </url>
        <?php } ?>
    <?php } ?>





</urlset>