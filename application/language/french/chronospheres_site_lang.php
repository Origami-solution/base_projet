<?php
/* Fichier de traduction du site Chronospheres.fr */

//VERSIONS


# 28/04/2015 Origami Solution


$lang["A venir"] = "A venir";
$lang["résultats"] = "résultats";
$lang[""] = "";
$lang[""] = "";
$lang[""] = "";






#MAJ 03/02/2015

$lang["message_ok__Votre_licence_est_a_jour"] = "Votre numéro de licence a bien été mis à jour";

$lang["message_error__veuillez_vous_connecter"] = "Pour accècer à cette page, vous devez être connecté";


#MAJ 03/02/2015



$lang["message_ok__inscription_gratuite_acceptee"] = "Votre inscription à bien été prise en compte, nous vous en remercions";



	#Menu des onglets du détail des événements
$lang["Description"] = "Description";
$lang["Liste des inscrits"] = "Liste des inscrits";
$lang["S'inscrire"] = "S'inscrire";
$lang["Les résultats"] = "Les résultats";




#MAJ 28/02/2015

$lang["page_evenements_h1_inscription_event_groupe"] = "Formulaire d'inscription de groupe";


$lang["Participant N°1"] = "Participant N°1";

$lang["Participant N°2"] = "Participant N°2";

$lang["Participant N°3"] = "Participant N°3";

$lang["Participant N°4"] = "Participant N°4";

$lang["Participant N°5"] = "Participant N°5";


$lang["Inscription obligatoire"] = "Inscription obligatoire";

$lang["Inscription facultative"] = "Inscription facultative";


$lang["Modal_inscription_input__licence_obtenue_participant"] = "Le participant possède t'il une licence ?";

$lang["Modal_inscription_input__licence_type_participant"] = "Type de licence";


$lang["Modal_inscription_input__entreprise"] = "Nom de l'entreprise";

$lang["page_evenements_detail_tab_Licencies_all"] = "Si tous les participants sont licenciés";




$lang["page_evenements_detail_tab_NonLicencies_all"] = "Si un ou plusieurs participants ne sont pas licenciés";





# Général 

$lang['titre_site'] = "Chronospheres";


# HEADER
$lang['header_sous_titre'] = "";
$lang['titre_menu_mobile'] = "Menu";




#MENU DU SITE
$lang['header_menu_top__MENU'] = "MENU"; //Bouton pour les smartphones

$lang['header_menu_top__Accueil'] = "Accueil";
$lang['header_menu_top__Quisommesnous'] = "Qui sommes nous ?";
$lang['header_menu_top__faq'] = "FAQ";
$lang['header_menu_top__Contact'] = "Contact";
$lang['header_menu_top__Evenements'] = "Les évènements";
$lang['header_menu_top__ConnexionInscription'] = "Connexion / inscription";


$lang['header_menu_top__Mon_compte'] = "Mon compte";

$lang['header_menu_top__Mes_informations'] = "Mes informations";
$lang['header_menu_top__Mes_instriptions'] = "Mes inscriptions";
$lang['header_menu_top__Mes_paiements'] = "Mes paiements";
$lang['header_menu_top__Deconnexion'] = "Déconnexion";



#FOOTER
$lang['footer_Titre_plan_du_site'] = "Plan du site";
$lang['footer_Titre_Contact'] = "Contact";
$lang['footer_Titre_Newsletter'] = "Lettre d'informations";

$lang['footer_Texte_Newsletter'] = "Vous souhaitez restez informé des événements publiés sur notre site, veuillez inscrire votre email ci-dessous.";
$lang['footer_Bouton_Newsletter'] = "Inscription";

$lang['footer_plan_Accueil'] = "Accueil";
$lang['footer_plan_Presentation'] = "Présentation";
$lang['footer_plan_evenements'] = "Les événements";
$lang['footer_plan_Inscription'] = "Inscription";
$lang['footer_plan_contacter'] = "Nous contacter";
$lang['footer_plan_CGU'] = "Les CGU";


$lang['footer_contact_Tel'] = "Tel : ";



#PAGE d'ACCUEIL
$lang['titre_page_accueil'] = "Bienvenue sur le site";
$lang['titre_h1_page_accueil'] = "Prochains événements";

$lang['page_accueil_bloc_information'] = "Cyclosportive, Cyclotourisme, Course à pied, Triathlon, Duathlon, Aquathlon, Trail.<br>Pour toutes vos compétitions nous <strong>adaptons nos solutions de chronométrages.</strong><br><br>

Inscription en lignes, classement en temps réel, puces actives ou passives, <br>nous utilisons les meilleures technologies du moment (race result) pour assurer le chronométrage.";



$lang['page_accueil_bouton_information_savoir_plus'] = "En savoir plus";





#BLOC MENU DROITE ACCUEIL / EVENEMENTS
$lang['panel_menuside__titre_cat'] = "Filtrer par catégories";
$lang['panel_menuside__lien_all_cat'] = "Toutes les catégories";

$lang['panel_menuside__titre_calendrier'] = "Calendrier des évènements";

$lang['panel_menuside__titre_carte'] = "Afficher les évènements par localité";
$lang['panel_menuside__titre_carou_events'] = "Liste des prochains événements";
$lang['panel_menuside__titre_facebook'] = "Suivez-nous sur Facebook";







#MODAL INSCRIPTION

$lang['Modal_inscription__Titre'] = "Connexion";
$lang['Modal_inscription__deja_membre'] = "Déjà membre ?";
$lang['Modal_inscription_placeholder__Adresse_email'] = "Adresse Email";
$lang['Modal_inscription_placeholder__Mot_de_passe'] = "Mot de passe";
$lang['Modal_inscription_Button__Connexion'] = "Connexion";
$lang['Modal_inscription_Button__Inscription'] = "Inscription";
$lang['Modal_inscription__devenir_membre'] = "Devenir membre pour s'inscrire aux événements";


$lang['Modal_inscription_Button__mot_de_passe_perdu'] = "Mot de passe perdu ?";

$lang['Modal_inscription__connexion_impossible_compe_innactif'] = "Compte Inactif, connexion impossible";
$lang['Modal_inscription__connexion_impossible_aucune_correspondance'] = "Votre Email et mot de passe ne correspondent pas";

$lang['Modal_inscription_input__Civilite'] = "Civilité";
$lang['Modal_inscription_input__Prenom'] = "Prénom";
$lang['Modal_inscription_input__Nom'] = "Nom";
$lang['Modal_inscription_input__email'] = "Email";
$lang['Modal_inscription_input__adresse_email'] = "Adresse email";
$lang['Modal_inscription_input__mot_de_passe'] = "Mot de passe";
$lang['Modal_inscription_input__verif_mot_de_passe'] = "Vérification du mot de passe";

$lang['Modal_inscription_input__Adresse'] = "Adresse";
$lang['Modal_inscription_input__cp'] = "Code postal";
$lang['Modal_inscription_input__Ville'] = "Ville";
$lang['Modal_inscription_input__Pays'] = "Pays";
$lang['Modal_inscription_input__Telephone'] = "Téléphone";
$lang['Modal_inscription_input__Jour'] = "Jour";
$lang['Modal_inscription_input__Mois'] = "Mois";
$lang['Modal_inscription_input__Annee'] = "Annee";

$lang['Modal_inscription_input__accepter_les'] = "Accèpter les ";

$lang['Modal_inscription_input__CGU'] = "CGU / CGV";

$lang['Modal_inscription_input__Date_de_naissance'] = "Date de naissance";
$lang['Modal_inscription_input__Nationalite'] = "Nationalité";
$lang['Modal_inscription_input__Club'] = "Club";
$lang['Modal_inscription_input__Equipe'] = "Equipe";
$lang['Modal_inscription_input__licence_obtenue'] = "Possèdez-vous une licence parmi celles cités ci-dessus ?";
$lang['Modal_inscription_input__licence_type'] = "Votre licence type de licence";
$lang['Modal_inscription_input__licence_numero'] = "Numéro de Licence";






$lang['Modal_inscription_Bouton_inscription_au_site'] = "Inscription au site";
$lang['Modal_inscription_Bouton_valider_mes_infos'] = "Valider mes informations";

#Page d'INSCRIPTION AU SITE

$lang['Page_inscription_h1'] = "Formulaire d'inscription";
$lang['Page_inscription_message_infos_facultatives'] = "(Informations facultatives)";





#Page RECUP MOT DE PASSE


$lang['titre_page_recup_mot_de_passe'] = "Récupération du mot de passe";
$lang['page_recup_mot_de_passe__input_email'] = "Adresse Email";

$lang['Alert_Message_OK_emailmotdepasse'] = "Vous allez recevoir un email avec votre mot de passse.";
$lang['Alert_Message_error_emailinconnu'] = "Email inconnu";
















#MODAL CGU
$lang['Modal_CGU__Titre'] = "Les CGU";
$lang['Modal_CGU__pas_de_contenu'] = "Aucune information pour le moment";
$lang['Modal_CGU__bouton_fermer'] = "Fermer";






#PAGE d EVENEMENTS
$lang['titre_page_evenements_accueil'] = "Les évènements";
$lang['page_evenements_h1_liste'] = "Liste des événements";

$lang['page_evenements_bloc_inscrits'] = "Inscrits : ";

$lang['page_evenements_bloc_attentes'] = "En attente : ";
$lang['page_evenements_bloc_Date'] = "Date : ";

$lang['page_evenements_bloc_Bouton_sinscrire'] = "S'inscrire";
$lang['page_evenements_bloc_Bouton_sinscrire_a_levenement'] = "S'inscrire à l'évènement";
$lang['page_evenements_bloc_Bouton_sinscrire_a_lepreuve'] = "S’inscrire à cette épreuve";
$lang['page_evenements_bloc_Bouton_suivre_direct'] = "Suivre le direct";
$lang['page_evenements_bloc_Bouton_inscription_a_venir'] = "Inscription à venir";
$lang['page_evenements_bloc_Bouton_inscription_closes'] = "Inscription closes";
$lang['page_evenements_bloc_Bouton_Resultats'] = "Résultats";
$lang['page_evenements_bloc_Bouton_Afficher_inscrits'] = "Afficher les inscrits";
$lang['page_evenements_bloc_Bouton_Afficher_details'] = "Afficher les détails";

$lang['page_evenements_bloc_alerte_aucun_evenement'] = "Aucun évènement pour le moment";


#PAGE d EVENEMENTS recherche catégorie
$lang['page_evenements_h1_categorie'] = "Liste des événements dans la catégorie";

#PAGE d EVENEMENTS recherche
$lang['page_evenements_h2_recherche_events'] = "Votre recherche parmi les événements";
$lang['page_evenements_h2_recherche_FAQ'] = "Votre recherche parmi la FAQ";
$lang['page_evenements_h2_recherche_FAQ_non'] = "Aucune information correspondante dans la FAQ";








#PAGE d EVENEMENTS detail
$lang['page_evenements_h1_detail_event'] = "Détail de l'événement";
$lang['page_evenements_h3_detail_information'] = "Informations";
$lang['page_evenements_h3_detail_Documents'] = "Documents";
$lang['page_evenements_h3_detail_Inscriptions_parcours'] = "Inscription aux parcours";


$lang['page_evenements_bouton_Afficher_le_reglement'] = "Afficher le règlement";
$lang['page_evenements_bouton_Afficher_le_document'] = "Afficher le document";


$lang['page_evenements_panel_partage_event_titre'] = "Partager cet événement";
$lang['page_evenements_panel_transaction_securise'] = "Transactions sécurisées";
$lang['page_evenements_panel_carte_titre'] = "Situation géographique";



$lang['page_evenements_detail_tab_Inscrits'] = "Inscrits";
$lang['page_evenements_detail_tab_Places'] = "Places";
$lang['page_evenements_detail_tab_Licencies'] = "Licenciés";
$lang['page_evenements_detail_tab_NonLicencies'] = "Non licenciés";
$lang['page_evenements_detail_tab_Frais_internet'] = "Frais inscription internet";
$lang['page_evenements_detail_tab_Participants'] = "Participants";
$lang['page_evenements_detail_tab_nombre_maxi'] = "Nombre maximum : ";
$lang['page_evenements_detail_tab_nombre_inscrit'] = "Nombre inscrits : ";
$lang['page_evenements_detail_tab_Tarifs'] = "Tarifs";
$lang['page_evenements_detail_tab_Tarifs_Non_licencies'] = "Tarifs Non licenciés";

$lang['page_evenements_detail_alert_aucun_parcours'] = "Aucun parcours pour le moment";
$lang['page_evenements_detail_alert_aucun_event'] = "Aucun évènement pour le moment";



#MODAL reglement de la course
$lang['page_evenements_detail_Modal_regelement_Titre'] = "Règlement de la course";
$lang['page_evenements_detail_Modal_regelement_bouton_nouvell_page'] = "Afficher le règlement dans une nouvelle page";
$lang['page_evenements_detail_Modal_regelement_bouton_Fermer'] = "Fermer";

#MODAL Documents de la course
$lang['page_evenements_detail_Modal_documents_Titre'] = "Document";
$lang['page_evenements_detail_Modal_documents_bouton_nouvell_page'] = "Afficher le document dans une nouvelle page";
$lang['page_evenements_detail_Modal_documents_bouton_Fermer'] = "Fermer";



#PAGE d EVENEMENTS Inscription
$lang['page_evenements_h1_inscription_event'] = "Formulaire d'inscription au parcours";
$lang['page_evenements_alerte_inscription_information_incompletes'] = "Les informations du formulaire ne sont pas complètes";

$lang['page_evenements_alerte_info_inscription_information'] = "<strong>Informations !</strong> <br>
  Si vous êtes déjà inscrit sur le site, veuillez vous ";


$lang['page_evenements_alerte_info_inscription_connecter'] = "connecter";
$lang['page_evenements_alerte_info_inscription_information_suite'] = "afin de récupérer vos informations, dans le cas d'un nouvel utilisateur un compte sera automatiquement créer";

$lang['page_evenements_inscription_information_licences_autorisés'] = "Les licences autorisées";
$lang['page_evenements_inscription_information_aucune_licence'] = "Aucune licence autorisé";

$lang['page_evenements_inscription_information_texte_CGU'] = "En cochant cette case vous accèpter le";
$lang['page_evenements_inscription_information_texte_CGU_suite'] = "règlement de la course";
$lang['page_evenements_inscription_information_texte_supplement_internet'] = "Supplément inscription internet : ";
$lang['page_evenements_inscription_information_Bouton_inscription_parcours'] = "Inscription au parcours";
$lang['page_evenements_inscription_information_alert_aucun_parcours'] = "Vous n'avez pas selectionner le parcours";



$lang['page_evenements_inscription_information_infos_licence'] = "Si vous possèdez une licence vous devez inscrire son type";
$lang['page_evenements_inscription_information_infos_licence_suite'] = "
Si vous possèdez une licence vous devez inscrire son numéro";


$lang['page_evenements_inscription_event_Panel_heading_infos_course'] = "Informations du parcours";

$lang['page_evenements_inscription_event_message_club_existe'] = "Si votre club existe dans nos propositions, merci de bien vouloir le sélectionner.";

$lang['page_evenements_inscription_event_message_Equipe_existe'] = "Si votre équipe existe dans nos propositions, merci de bien vouloir la sélectionner.";









#PAGES DE ESPACE MEMBRE
$lang['titre_page_espace_membre'] = "Espace membre";





#PAGE de CONTACT
$lang['titre_page_contact'] = "Contact";

$lang['titre_h1_contact'] = "Nous contacter";
$lang['panel_informations_titre'] = "Information de contact";

$lang['panel_informations_raison'] = "Raison Sociale : ";
$lang['panel_informations_Representant'] = "Représentant : ";
$lang['panel_informations_tel'] = "Tel : ";

$lang['panel_carte_titre'] = "Situation géographique";


$lang['titre_h2_formulaire_titre'] = "Formulaire de contact";
$lang['contact_formulaire_sous_titre'] = "Vous souhaitez nous contacter ou faire une demande de devis, veuillez utiliser notre formulaire de contact ci-dessous. Nous vous contacterons dans les plus bref délais";

$lang['contact_formulaire_input__Nom'] = "Nom";
$lang['contact_formulaire_input__prenom'] = "Prénom";
$lang['contact_formulaire_input__Email'] = "Email";
$lang['contact_formulaire_input__Email_infos'] = "Votre email nous permettra de vous contacter";

$lang['contact_formulaire_input__tel'] = "Téléphone";
$lang['contact_formulaire_input__Objet'] = "Objet";
$lang['contact_formulaire_input__Message'] = "Message";
$lang['contact_formulaire_input__Message_infos'] = "Merci de me donner le plus d'informations possible afin de pouvoir répondre au mieux à votre demande";

$lang['contact_formulaire_Bouton__Envoyer_message'] = "Envoyer le message";



//message qui s'inscrit lorsque que l'on souhaite afficher une recherche vide
$lang['contact_formulaire__Message_erreur'] = "Aucune information de contact pour le moment, cette page est en cours d'édition, veuillez revenir plus tard";






#Page d'inscription


$lang['titre_page_inscription'] = "Inscription au site";



#Page CGU
$lang['titre_page_cgu'] = "CGU";
$lang['page_cgu_message_aucune_info'] = "Aucune information pour le moment";


 
/* Maintenance */

$lang['maintenance_titre'] = "Maintenance en cours !";// titre de la page de maintenance
$lang['maintenance_message'] = "Le site est actuellement en cours de maintenance, veuillez revenir plus tard.<br>Merci."; // message de maintenance qui est sous le titre de la page



#erreurs
$lang['erreur'] = "Une erreur est survenue";




#PARTIE MON COMPTE

#Page espace Membre
$lang['page_membre_h2_accueil_mon_compte'] = "Mon compte";
$lang['page_membre_panel_accueil_titre'] = "Bienvenue dans votre espace membre";
$lang['page_membre_panel_accueil_message'] = "Vous pouvez mettre à jour vos informations personnelles dans cette espace ainsi que vérifier vos inscriptions.";

$lang['page_membre_panel_accueil_dernieres_inscription'] = "Les dernières inscriptions";

$lang['page_membre_panel_accueil_dernieres_inscription_le'] = "Inscription le ";
$lang['page_membre_panel_accueil_dernieres_inscription_Valide'] = "Validé";
$lang['page_membre_panel_accueil_dernieres_inscription_Rembousé'] = "Rembousé";

$lang['page_membre_panel_accueil_dernieres_Bouton_all_inscriptions'] = "Voir toutes mes inscriptions";

$lang['page_membre_panel_accueil_dernieres_Bouton_all_paiements'] = "Voir tout mes paiements";




#page espace membre mes informations

$lang['page_membre_panel_mes_informations_titre'] = "Mes informations";


$lang['page_membre_panel_mes_informations_message'] = "Vous pouvez éditer vos informations";


#page espace_membre le inscriptions aux events

$lang['page_membre_panel_inscriptions_titre'] = "La liste de vos inscriptions aux évènements";

$lang['page_membre_panel_inscriptions_tableau_titre_parcours'] = "Parcours";
$lang['page_membre_panel_inscriptions_tableau_titre_Participant'] = "Participant";
$lang['page_membre_panel_inscriptions_tableau_titre_Statut'] = "Statut";
$lang['page_membre_panel_inscriptions_tableau_Bouton_paiement'] = "Paiement";
$lang['page_membre_panel_inscriptions_tableau_Bouton_annuler'] = "Annuler l'inscription";
$lang['page_membre_panel_inscriptions_tableau_alerte_aucune_info'] = "Aucune inscription pour le moment";


#page espace_membre les paiements

$lang['page_membre_panel_paiements_titre'] = "La liste de vos paiements";
$lang['page_membre_panel_paiements_message'] = "Liste des inscriptions règlées";
$lang['page_membre_paiements_Tableau_Transaction'] = "Numéro de transaction :";
$lang['page_membre_paiements_Tableau_Reglement_le'] = "Règlement le ";
$lang['page_membre_paiements_Tableau_aucune_commande'] = "Vous n'avez pas de commande.";


 




#LES ALERTES

//Inscription au site OK
$lang['message_ok__Inscription_valide'] = "Votre inscription à bien été prise en compte, vous allez recevoir un email de confirmation. Vous pouvez dès à présent vous inscrire aux évènements"; 

//message vous vous etes bien deconnecter
$lang['message_ok__Deconnexion_ok'] = "Vous avez été déconnecté. A bientôt"; 

//message vous vous etes bien connecter
$lang['message_ok__vous_etes_connecter'] = "Vous êtes bien connecté"; 

//message vos informations du compte on bien été modifiés
$lang['message_ok__modification_compte'] = "Vos informations ont bien été modifiés"; 

//message le client vient de mettre une épreuve au panier il est en attente de reglement

$lang['message_ok__inscription_epreuve_ok__attente_de_reglement'] = "Afin de valider votre inscription, veuillez effectuer le règlement ci-dessous";

//message qui confirme la bonnesuppression de l'inscription de l'utilisateur
$lang['message_ok__suppression_inscription'] = "Votre inscription a bien été supprimé, vous n'êtes plus inscrit";

//message qui confirme l'envoi de message de contact
$lang['message_ok__Message_envoyer_ok'] = "Merci, votre message a bien été envoyé.";

//message qui s'inscrit lorque que l'on soihaite afficher le formulaire mais que les inscritpion sont fermée
$lang['message_error__inscription_fermee'] = "Les inscriptions sont closes.";


//message qui s'inscrit lorsque que l'on souhaite afficher une recherche vide
$lang['message_error__veuillez_saisir_recherche'] = "Veillez saisir un mot de recherche";

//message qui s'inscrit lorsque que l'on annule le paiement bancaire
$lang['message_error__paiement_annule'] = "Votre paiement n'a pas abouti, veuillez réessayer";

//message qui s'inscrit lorsque que l'on annule le paiement bancaire
$lang['message_ok__paiement_attente'] = "Votre paiement est en attente de validation";
$lang['message_error__paiement_refuser'] = "Votre paiement à été refusé";

$lang['message_error__paiement_accepter'] = "Votre paiement est accepté, merci pour votre achat, vous allez recevoir un email de confirmation.";

//message qui s'inscrit lorsque que l'on s'inscrit a la newsletter
$lang['message_ok__inscription_newsletter_ok'] = "Vous avez été inscrit à notre lettre d'information. Merci";

//message qui s'inscrit lorsque que l'on se désinscrit a la newsletter
$lang['message_ok__desinscription_newsletter_ok'] = "Votre email a bien été retirer de notre base de donnée.";



# LES UTILISATEURS 


//$lang['admin_users_titre'] = "Les utilisateurs";
