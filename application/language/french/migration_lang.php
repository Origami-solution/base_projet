<?php

$lang['migration_none_found']			= "Aucune mise à jour.";
$lang['migration_not_found']			= "La mise à jour n'a pas été trouvée";
$lang['migration_multiple_version']		= "Plusieurs mise à jours pour le mème numéro : %d.";
$lang['migration_class_doesnt_exist']	= "La class de mise à jour \"%s\" n'existe pas";
$lang['migration_missing_up_method']	= "La class de mise à jour 'UP' \"%s\" est introuvable";
$lang['migration_missing_down_method']	= "La class de mise à jour 'DOWN' \"%s\"  est introuvable";
$lang['migration_invalid_filename']		= "La mise à jour \"%s\" est un nom invalide";


/* End of file migration_lang.php */
/* Location: ./system/language/english/migration_lang.php */