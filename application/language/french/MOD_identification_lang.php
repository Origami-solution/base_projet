<?php
/* Fichier de traduction du site */



#MODULE CONNEXION / INSCRIPTION

$lang['identification_bouton'] = "Connexion / Inscription" ;
$lang['identification_modal_titre'] = "Identification" ;
$lang['identification_modal_fermer'] = "Fermer" ;

$lang['identification_modal_tab_titre_connexion'] = "Se connecter" ;
$lang['identification_modal_tab_titre_inscription'] = "S'inscrire" ;

	#connexion
$lang['identification_modal_input_email'] = "Email" ;
$lang['identification_modal_input_pass'] = "Mot de passe" ;

$lang['identification_modal_message_pas_reconnu'] = "L'email et mot de passe associés ne correspondent pas à un compte existant, veuillez vérifier vos informations." ;
$lang['identification_modal_message_compte_inactif'] = "Votre compte est inactif, veuillez contacter l'administrateur du site pour débloquer la situation." ;

$lang['identification_modal_message_compte_bloquer'] = "Votre compte est bloquer, veuillez contacter l'administrateur du site pour débloquer la situation." ;


$lang['identification_modal_message_compte_creer'] = "Votre compte à bien été créer, vous aller recevoir un email de confirmation.<br><br>Vous pouvez vous connecter avec vos identifiants." ;

$lang['identification_modal_message_demande_dactivation_envoyer'] = "Votre demande de réactivation à bien été envoyé, celle-ci sera traité dans les plus bref délais.<br>Vous serez informé par email." ;
$lang['identification_modal_message_demande_dactivation_erreur'] = "Une erreur est survenue, votre demande de réactivation n'à pas été envoyé, votre compte est introuvable, veuillez contacter l'administrateur depuis la page de contact." ;


	#message Email de création du compte
$lang['identification_email_inscription_sujet']  = "Vos identifiants de connexion sur %1\$s" ;

//$lang['identification_email_inscription_EMAIL_FROM'] = "contact@origami-solution.com" ;
//$lang['identification_email_inscription_EMAIL_BCC'] = "winprod@sfr.fr" ;




$lang['identification_email_inscription_A'] = "Merci de votre inscription sur notre site : <a href=\"".site_url()."\">%1\$s</a>";
$lang['identification_email_inscription_B'] = "Voici vos identifiants de connexion :";
$lang['identification_email_inscription_C_identifiant'] = "Identifiant" ;
$lang['identification_email_inscription_C_mot_de_passe'] = "Mot de passe" ;
$lang['identification_email_inscription_D_information_confidentielles'] = "Important, vos identifiants sont personnels et vous seront demandés pour chaque connexion sur le site." ;
$lang['identification_email_inscription_E_remerciement'] = "Toute l'équipe vous remercie." ;







$lang['identification_email_suppression_compte_A'] = "Suppression du compte";
$lang['identification_email_suppression_compte_B'] = "Vous avez souhaitez supprimer votre compte sur le site <a href=\"".site_url()."\">%1\$s</a>, votre demande à bien été prise en compte.";
$lang['identification_email_suppression_compte_C'] = "Si toutefois vous souhaitez revenir sur votre demande, vous disposez de 15 jours pour annuler cette demande, pour cela veuillez vous rendre dans votre espace membre pour réactiver votre compte." ;

$lang['identification_email_suppression_compte_D'] = "En espérant vous revoir prochainement sur notre site." ;







$lang['identification_email_compte_bloquer_A'] = "Votre compte à été bloquer";
$lang['identification_email_compte_bloquer_B'] = "Bonjour, nous vous informons que votre compte sur le site %1\$s a été bloquer, vous pouvez faire une demande de réactivation en vous reconnectant.";
$lang['identification_email_compte_bloquer_C'] = "Rappel de vos identifiants :";
$lang['identification_email_compte_bloquer_Remerciement'] = "En espérant vous revoir prochainement sur notre site.<br>".site_url();






$lang['identification_email_compte_supprimer_A'] = "Votre compte à été supprimer";
$lang['identification_email_compte_supprimer_B'] = "Bonjour, nous vous informons que votre compte sur le site %1\$s a été supprimer, toutes les informations vous concernant ont été effacées.";
$lang['identification_email_compte_supprimer_Remerciement'] = "En espérant vous revoir prochainement sur notre site.<br>".site_url();






$lang['identification_email_compte_active_A'] = "Activation du compte";
$lang['identification_email_compte_active_B'] = "Bonjour, nous vous informons que votre compte sur le site %1\$s a été activé.";
$lang['identification_email_compte_active_C'] = "Rappel de vos identifiants :";
$lang['identification_email_compte_active_Remerciement'] = "Toute l'équipe vous remercie.<br>".site_url();





	#Mot de passe perdu



$lang['identification_modal_input_email_perdu'] = "Email" ;
$lang['identification_modal_bouton_pass_perdu'] = "Récupérer mon mot de passe" ;
$lang['identification_modal_message_generation_pass'] = "Vous allez recevoir un email vos identifiants." ;
$lang['identification_modal_message_pas_compte_existant'] = "Cet email ne correspond avec aucun compte." ;






#message email mot de passe perdu


$lang['identification_email_recuppass_sujet']  = "Vos identifiants de connexion sur %1\$s" ;

//$lang['identification_email_recuppass_EMAIL_FROM'] = "contact@origami-solution.com" ;
//$lang['identification_email_recuppass_EMAIL_BCC'] = "winprod@sfr.fr" ;



$lang['identification_email_recuppass_A'] = "Vous avez demandé vos identifiants sur : <a href=\"".site_url()."\">%1\$s</a>";
$lang['identification_email_recuppass_B'] = "Voici vos identifiants de connexion :";
$lang['identification_email_recuppass_C_identifiant'] = "Identifiant" ;
$lang['identification_email_recuppass_C_mot_de_passe'] = "Mot de passe" ;
$lang['identification_email_recuppass_D_information_confidentielles'] = "Important, vos identifiants sont personnels et vous seront demandés pour chaque connexion sur le site." ;
$lang['identification_email_recuppass_E_remerciement'] = "Toute l'équipe vous remercie." ;





//"Le %2\$s est plein de %1\$s. J'aime les %1\$s.";
	#inscription
$lang['identification_modal_input_email_confirmation'] = "Confirmer votre email" ;
$lang['identification_modal_input_civilite'] = "Civilité" ;
$lang['identification_modal_input_civilite_mr'] = "Mr" ;
$lang['identification_modal_input_civilite_mme'] = "Mme" ;
$lang['identification_modal_input_prenom'] = "Prénom" ;
$lang['identification_modal_input_nom'] = "Nom" ;
$lang['identification_modal_input_pseudo'] = "Pseudo" ;



$lang['identification_modal_se_souvenir'] = "Se souvenir de moi" ;

$lang['identification_modal_bouton_connexion'] = "Se connecter" ;
$lang['identification_modal_bouton_inscription'] = "S'inscrire" ;



$lang['identification_modal_perdu'] = "Mot de passe perdu ?" ;






# menu mon compe


$lang['identification_menudropdown_espace_membre'] = "Espace membre" ;










# LES UTILISATEURS 


//$lang['admin_users_titre'] = "Les utilisateurs";
