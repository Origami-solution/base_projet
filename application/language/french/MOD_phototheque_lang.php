<?php
/* Fichier de traduction du module de Phototheque */




#MODULE     PHOTOTHEQUE

$lang['phototheque_titre_accueil'] = "La photothèque" ;

$lang['phototheque_titre_breadcrumb_album'] = "Album" ;
$lang['phototheque_titre_breadcrumb_recherche'] = "Recherche" ;




$lang['phototheque_titre_accueil_galere_photo'] = "Galerie photo" ;
$lang['phototheque_titre_recherche'] = "Rechercher" ;
$lang['phototheque_titre_panel_albums_recements_ajoutes'] = "Les albums récements ajoutés" ;



$lang['phototheque_message_accueil_galerie'] = "Retrouvez les évènements de notre commune immortalisés dans la photothèque." ;
$lang['phototheque_message_recherche'] = "Votre recherche." ;
$lang['phototheque_message_recherche_resultat'] = "Il n'y a pas d'album correspondant à votre recherche." ;


$lang['phototheque_message_accueil_galerie_aucun_album'] = "Il n'y a pas d'album pour le moment." ;
$lang['phototheque_message_accueil_galerie_aucune_categorie'] = "Il n'y a pas de catégorie." ;


$lang['phototheque_message_mentions_legales'] = "%1\$s Informations légales et règlement %2\$s des photos publiés sur ce site" ;
$lang['phototheque_message_contact'] = "Vous possèdez des photos qui pourraient être présentes dans notre photothèque, veuillez %1\$s nous contacter %2\$s" ;


$lang['phototheque_texte_detail_nbr_photo'] = "photos" ;

















/*
$lang['blog_titre_categories'] = "Catégories" ;
$lang['blog_titre_recherche'] = "Recherche" ;
$lang['blog_titre_auteur'] = "Auteur" ;

$lang['blog_titre_breadcrumb_categorie'] = "Catégorie" ;
$lang['blog_titre_breadcrumb_auteur'] = "Auteur" ;
$lang['blog_titre_breadcrumb_recherche'] = "Recherche" ;



$lang['blog_message_aucun_article_correspondant'] = "Il n'y a aucun article à cette adresse" ;
$lang['blog_message_aucun_article_correspondant_recherche'] = "Il n'y a aucun article qui corresponde à votre recherche" ;
$lang['blog_message_resultat_recherche'] = "Il y a <strong>%1\$s</strong> article(s) correspondant(s) à votre recherche <strong>\"%2\$s\"</strong> " ;
$lang['blog_message_resultat_categorie'] = "Ci dessous les articles correspondants à la catégorie <strong>%1\$s</strong>" ;
$lang['blog_message_resultat_auteur'] = "Ci dessous les articles publiés par <strong>%1\$s</strong>" ;
 


$lang['blog_article_date_post'] = "Ecrit le" ;
$lang['blog_article_lien'] = "Lire la suite" ;
$lang['blog_article_retour_aux_articles'] = "Retour aux articles" ;




#page article

$lang['blog_article_titre_photos'] = "Les photos" ;
$lang['blog_article_titre_documents'] = "Les documents" ;
$lang['blog_article_bouton_telecharger_documents'] = "Télécharger" ;


#page article Commentaires



$lang['blog_article_commentaire_date_post'] = "Posté le : " ;



$lang['blog_article_commentaire_tooltip_modif_message'] = "Modifier le message" ;
$lang['blog_article_commentaire_tooltip_supprimer_message'] = "Supprimer le message" ;


$lang['blog_article_bouton_ajouter_commentaires'] = "Ajouter un commentaire" ;
$lang['blog_article_bouton_se_connecter_ajouter_commentaires'] = "Se connecter pour ajouter un commentaire" ;
$lang['blog_article_message_se_connecter_ajouter_commentaires'] = "Pour ajouter un commentaire vous devez être connecter." ;

$lang['blog_article_message_commentaires_bien_ajoute'] = "Votre commentaire a bien été ajouté." ;
$lang['blog_article_message_commentaires_bien_modifier'] = "Votre commentaire a bien été modifié." ;
$lang['blog_article_message_commentaires_aucun_commentaire'] = "Aucun commentaire, soyez le premier à poster un commentaire !" ;


$lang['blog_article_message_commentaires_titre_modal_modifier'] = "Modifier le commentaire" ;
$lang['blog_article_message_commentaires_bouton_modal_modifier'] = "Valider les modifications" ;

$lang['blog_article_input_label_message'] = "Votre message" ;
$lang['blog_article_input_label_annuler_message'] = "Annuler" ;
$lang['blog_article_input_label_publier_message'] = "Ajouter mon commentaire" ;

*/