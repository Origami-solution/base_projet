<?php
#MODULE     PROFIL





$lang['profil_titre'] = "Mes informations" ;
$lang['profil_titre_mon_compte'] = "Mon compte" ;
$lang['profil_titre_informations_personnelles'] = "Mes informations personnelles" ;
$lang['profil_titre_lettre_informations'] = "Lettre d'information" ;
$lang['profil_titre_motdepasse'] = "Mon mot de passe" ;
$lang['profil_titre_supprimer_compte'] = "Supprimer mon compte" ;


#bouton
$lang['profil_bouton_modif_informations'] = "Modifier les informations" ;
$lang['profil_bouton_modif'] = "Modifier" ;
$lang['profil_bouton_modif_pass'] = "Modifier le mot de passe" ;
$lang['profil_bouton_modif_supprimer_compte'] = "Supprimer mon compte" ;
$lang['profil_bouton_Annuler_supprimer_compte'] = "Annuler la suppression du compte" ;






#text

$lang['profil_text_email_invisible'] = "(Votre email n'est pas visible par les visiteurs)." ;
$lang['profil_text_inscrit_le'] = "Inscrit le" ;
$lang['profil_text_Adresse'] = "Adresse" ;
$lang['profil_text_Tels'] = "Téléphones" ;
$lang['profil_text_datedenaissance'] = "Date de naissance" ;
$lang['profil_texte_infos_recevoir_lettre_information'] = "Recevoir par email la lettre d'information de la commune." ;
$lang['profil_text_votre_mot_de_passe'] = "Votre mot de passe vous permet de vous connecter au site." ;
$lang['profil_text_supprimer_compte'] = "Vous souhaitez supprimer votre compte, veuillez remplire les champs suivants puis cliquer sur le bouton supprimer le compte, votre compte sera inactif pendant 15 jours durant cette période vous pourrez si vous le souhaitez, le réactiver. <br>Au dela de 15 jours, votre compte sera définitivement supprimer, toutes les informations reative à votre compte seront effacés." ;
$lang['profil_text_suppression_en_cours'] = "Suppression du compte en cours" ;
$lang['profil_text_suppression_en_cours_active'] = "Vous avez activer la suppression du compte, celui-ci <strong>sera supprimer le %1\$s </strong>" ;



#alert

$lang['profil_message_alert_warning_erreur_mot_de_passe'] = "Erreur de mot de passe" ;
$lang['profil_message_alert_warning_compte_inactif'] = "Votre compte est inactif," ;
$lang['profil_message_alert_warning_bouton_reactiver_compte'] = "Réactiver le compte" ;



$lang['profil_message_alert_success_compte_actif'] = "Votre compte est actif" ;













#formulaire


$lang['profil_input_motdepsse'] = "Mot de passe" ;
$lang['profil_input_case'] = "Case a cocher" ;

$lang['profil_label_votre_mot_de_passe'] = "Votre mot de passe" ;

$lang['profil_label_check_supprimer'] = "Je souhaite supprimer mon compte ainsi que toutes les informations personnelles" ;










$lang['profil_input_grocery_avatar'] = "Avatar" ;
$lang['profil_input_grocery_civilite'] = "Civilité" ;
$lang['profil_input_grocery_prenom'] = "Prénom" ;
$lang['profil_input_grocery_nom'] = "Nom" ;
$lang['profil_input_grocery_pseudo'] = "Pseudo" ;
$lang['profil_input_grocery_email'] = "Email" ;

$lang['profil_input_grocery_adresse'] = "Adresse" ;
$lang['profil_input_grocery_code_postal'] = "Code postal" ;
$lang['profil_input_grocery_ville'] = "Ville" ;
$lang['profil_input_grocery_pays'] = "Pays" ;
$lang['profil_input_grocery_tel_fixe'] = "Téléphone Fixe" ;
$lang['profil_input_grocery_tel_portable'] = "Téléphone Portable" ;
$lang['profil_input_grocery_date_de_naissance'] = "Date de naissance" ;



$lang['profil_input_grocery_motdepasse'] = "Nouveau mot de passe<br><span class=\"text-muted\">Entre 5 et 12 caractères</span>" ;
