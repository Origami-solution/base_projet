<?php
/* Fichier de traduction du formulaire evergreenchallenge */

/*
			FR

*/



#MAJ 07/04/2015

$lang["message_email_de_paiement_envoyé"] = "Nous vous avons envoyé un email avec un lien pour le paiement";


#MAJ 01/04/2015
$lang["message_page_paiement"] = "Votre inscription est pour l'instant préenregistrée,<br><br> Afin de confirmer votre inscription immédiatement, veuillez effectuer votre paiement en ligne, en utilisant le bouton de paiement « Payer MAINTENANT».<br><br>
Si toutefois vous souhaitez effectuer votre règlement plus tard, veuillez utiliser le bouton « Payer PLUS TARD » 
<br><br>
Notre système enregistrera vos données et vous enverra un email avec un lien pour effectuer votre paiement.";


$lang["message_page_paiement_signature"] = "Nous sommes impatients de vous compter parmi nos athlètes au départ de l'édition 2015 !";


$lang["message_email_demande_de_paiement_A"] = "Votre inscription est pour l'instant préenregistrée, il vous faut régler votre paiement en ligne afin de la confirmer définitivement.<br>
Afin d'effectuer ce règlement, veuillez utiliser le lien de paiement suivant:";


$lang["message_email_demande_de_paiement_B"] = "Merci d'avance pour votre paiement, nous sommes très heureux de vous compter parmi nos athlètes au départ de cette première édition!";

$lang["message_email_demande_de_paiement_Liendepaiement"] = "Lien de paiement";




#MAJ 07/03/2015

$lang["email_expediteur_emails"] = "registration@evergreen-endurance.com";




#email confirmation de paiement 228 RELAY


$lang['Email_confirm_paiement_228R__SUJET'] = "Confirmation de paiement";

//sprintf($format, $num, $location);
$lang['Email_confirm_paiement_228R__TEXT_1'] = "Cher %1\$s %2\$s %3\$s, nous confirmons le paiement de votre inscription pour Evergreen 228 Relais qui aura lieu le 12 septembre 2015.";



$lang['Email_confirm_paiement_228R__TEXT_2'] = "Vous recevrez bientôt un email de confirmation.";

$lang['Email_confirm_paiement_228R__TEXT_3'] = "Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.";








#Mail de confirmation de préinscription 228 RELAY



$lang["Email_confirm_preinscription_228R_SUJET"] = "confirmation d’inscription";

$lang['Email_confirm_preinscription_228R_TEXT_1'] = "Cher %1\$s, <br> Wow....Félicitations, vous êtes inscrit! <strong>Une grande aventure vous attend...</strong>";

$lang['Email_confirm_preinscription_228R_TEXT_2'] = "Nous confirmons que vous êtes inscrit pour Evergreen 228 Relais qui aura lieu le 12 septembre 2015 !";


$lang['Email_confirm_preinscription_228R_TEXT_3'] = "Vous recevrez très prochainement un email vous invitant a créer votre profil d’athlète sur notre site internet. Ce lien vous permettra de télécharger les documents relatifs a votre course ainsi que d’autres « goodies » de nos partenaires, y compris l’application TriFit training. Vous aurez la possibilité de renseigner les détails de votre profile, d’y ajouter une photo et de suivre vos progrès de collecte de fonds.* Nous souhaitons vivement que votre expérience soit une aventure personnalisée alors n’oubliez pas de prendre le temps de nous dire qui vous êtes et de quoi vous êtes capable !";


$lang['Email_confirm_preinscription_228R_TEXT_4'] = "*(Vous aurez la possibilité de partager ce lien avec vos réseaux personnels et sociaux afin de vous aider dans vos collectes de fonds).";


$lang['Email_confirm_preinscription_228R_TEXT_5'] = "
Que vous reste-t-il à faire?
<br>
<ul>
<li>Collectez des fonds autour de vous et recevez un remboursement partiel ou total de vos frais d’inscription.</li>
<li>Entrainez vous car cela va être une longue journée!</li>
</ul>
<br>
N’oubliez pas non plus de jeter de temps en temps un coup d’œil sur notre site internet ou sur notre page Facebook pour plus d’information.";



$lang['Email_confirm_preinscription_228R_TEXT_6'] = "Enfin, …<br>
Nous avons pris la liberté de vous insérer dans notre liste d’athlètes afin que nous puissions vous envoyer des informations utiles sur la course en temps et en heure. <br><br>Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.
";





























#Mail de confirmation de préinscription 228



$lang["Email_confirm_preinscription_228_SUJET"] = "confirmation d’inscription";

$lang['Email_confirm_preinscription_228_TEXT_1'] = "Cher %1\$s, <br> Wow....Félicitations, vous êtes inscrit! <strong>Une grande aventure vous attend...</strong>";

$lang['Email_confirm_preinscription_228_TEXT_2'] = "Nous confirmons que vous êtes inscrit pour Evergreen 228 qui aura lieu le 12 septembre 2015 !";


$lang['Email_confirm_preinscription_228_TEXT_3'] = "Vous recevrez très prochainement un email vous invitant a créer votre profil d’athlète sur notre site internet. Ce lien vous permettra de télécharger les documents relatifs a votre course ainsi que d’autres « goodies » de nos partenaires, y compris l’application TriFit training. Vous aurez la possibilité de renseigner les détails de votre profile, d’y ajouter une photo et de suivre vos progrès de collecte de fonds.* Nous souhaitons vivement que votre expérience soit une aventure personnalisée alors n’oubliez pas de prendre le temps de nous dire qui vous êtes et de quoi vous êtes capable !";


$lang['Email_confirm_preinscription_228_TEXT_4'] = "*(Vous aurez la possibilité de partager ce lien avec vos réseaux personnels et sociaux afin de vous aider dans vos collectes de fonds).";


$lang['Email_confirm_preinscription_228_TEXT_5'] = "
Que vous reste-t-il à faire?
<br>
<ul>
<li>Collectez des fonds autour de vous et recevez un remboursement partiel ou total de vos frais d’inscription.</li>
<li>Entrainez vous car cela va être une longue journée!</li>
</ul>
<br>
N’oubliez pas non plus de jeter de temps en temps un coup d’œil sur notre site internet ou sur notre page Facebook pour plus d’information.";



$lang['Email_confirm_preinscription_228_TEXT_6'] = "Enfin, …<br>
Nous avons pris la liberté de vous insérer dans notre liste d’athlètes afin que nous puissions vous envoyer des informations utiles sur la course en temps et en heure. <br><br>Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.
";







#email confirmation de paiement 228


$lang['Email_confirm_paiement_228__SUJET'] = "Confirmation de paiement";

//sprintf($format, $num, $location);
$lang['Email_confirm_paiement_228__TEXT_1'] = "Cher %1\$s, nous confirmons le paiement de votre inscription pour Evergreen 228 qui aura lieu le 12 septembre 2015.";



$lang['Email_confirm_paiement_228__TEXT_2'] = "Vous recevrez bientôt un email de confirmation.";

$lang['Email_confirm_paiement_228__TEXT_3'] = "Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.";












#email confirmation de paiement 118


$lang['Email_confirm_paiement_118__SUJET'] = "Confirmation de paiement";

//sprintf($format, $num, $location);
$lang['Email_confirm_paiement_118__TEXT_1'] = "Cher %1\$s, nous confirmons le paiement de votre inscription pour Evergreen 118 qui aura lieu le 12 septembre 2015.";



$lang['Email_confirm_paiement_118__TEXT_2'] = "Vous recevrez bientôt un email de confirmation.";

$lang['Email_confirm_paiement_118__TEXT_3'] = "Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.";





#Mail de confirmation de préinscription 118



$lang["Email_confirm_preinscription_118_SUJET"] = "confirmation d’inscription";

$lang['Email_confirm_preinscription_118_TEXT_1'] = "Cher %1\$s, <br> Wow....Félicitations, vous êtes inscrit! <strong>Une grande aventure vous attend...</strong>";

$lang['Email_confirm_preinscription_118_TEXT_2'] = "Nous confirmons que vous êtes inscrit pour Evergreen 118 qui aura lieu le 12 septembre 2015 !";


$lang['Email_confirm_preinscription_118_TEXT_3'] = "Vous recevrez très prochainement un email vous invitant a créer votre profil d’athlète sur notre site internet. Ce lien vous permettra de télécharger les documents relatifs a votre course ainsi que d’autres « goodies » de nos partenaires, y compris l’application TriFit training. Vous aurez la possibilité de renseigner les détails de votre profile, d’y ajouter une photo et de suivre vos progrès de collecte de fonds.* Nous souhaitons vivement que votre expérience soit une aventure personnalisée alors n’oubliez pas de prendre le temps de nous dire qui vous êtes et de quoi vous êtes capable !";


$lang['Email_confirm_preinscription_118_TEXT_4'] = "*(Vous aurez la possibilité de partager ce lien avec vos réseaux personnels et sociaux afin de vous aider dans vos collectes de fonds).";


$lang['Email_confirm_preinscription_118_TEXT_5'] = "
Que vous reste-t-il à faire?
<br>
<ul>
<li>Collectez des fonds autour de vous et recevez un remboursement partiel ou total de vos frais d’inscription.</li>
<li>Entrainez vous car cela va être une longue journée!</li>
</ul>
<br>
N’oubliez pas non plus de jeter de temps en temps un coup d’œil sur notre site internet ou sur notre page Facebook pour plus d’information.";



$lang['Email_confirm_preinscription_118_TEXT_6'] = "Enfin, …<br>
Nous avons pris la liberté de vous insérer dans notre liste d’athlètes afin que nous puissions vous envoyer des informations utiles sur la course en temps et en heure. <br><br>Pour toute question relative à votre inscription merci de nous contacter à l’adresse email suivante : registration@evergreen-endurance.com. Pour toute autre question, veuillez nous contacter à info@evergreen-endurance.com.
";













$lang['Bouton_Regler_en_ligne'] = "Régler MAINTENANT";
$lang['Bouton_Regler_en_ligne_plus_tard'] = "Régler PLUS TARD";



$lang['votre_inscription_est_confirmee'] = "Votre inscription est confirmée";








#MAJ 06/03/2015





$lang['message_error__paiement_annule'] = "Votre paiement n'a pas abouti, veuillez réessayer";

$lang['message_ok__paiement_attente'] = "Votre paiement est en attente de validation";

$lang['message_error__paiement_refuser'] = "Votre paiement à été refusé";

$lang['message_error__paiement_accepter'] = "Votre paiement est accepté, merci pour votre achat, vous allez recevoir un email de confirmation.";





$lang['Valider'] = "Valider";

$lang['code_promo_invalide'] = "Ce code n'est pas valide";
$lang['code_promo_indisponible'] = "Ce code n'est plus disponible";
$lang['code_promo_add_success'] = "Votre code a été ajouté avec success";

$lang['code_promo_erreur_article'] = "Votre code n'est pas valable pour cette course";

$lang['code_promo_erreur_utilisateur'] = "Vous n'êtes pas autorisé a utiliser ce code";



#MAJ 03/03/2015


$lang['Licencié : '] = "Licencié: ";
$lang['Non Licencié : '] = "Non licencié: ";


$lang['egc_input_date_de_naissance'] = "Date de naissance*";


$lang['egc_input_licence_type'] = "Type de licence";

$lang['egc_input_licence_type__0'] = "Pas de licence - Achat Pass Compétition pour";
$lang['egc_input_licence_type__1'] = "FFTRI";
$lang['egc_input_licence_type__2'] = "ITU";

$lang['egc_input_licence_num'] = "Numéro de licence";

$lang['egc_input_club'] = "Club";
$lang['egc_input_equipe'] = "Equipe";

$lang['egc_input_connu'] = "Comment nous avez-vous connu?*";

$lang['egc_input_connu__Magasine'] = "Magasine";
$lang['egc_input_connu__Website'] = "Website";
$lang['egc_input_connu__Bouche_oreille'] = "Bouche à oreille";
$lang['egc_input_connu__Facebook'] = "Facebook";
$lang['egc_input_connu__Autre'] = "Autre";

$lang['egc_input_connu_autre_infos'] = "Précisez comment nous avez-vous connu ?";

$lang['egc_etape2_exi_7'] = "Accompli Evergreen 118 et / ou Evergreen 228";



$lang['Choisir une méthode de qualification'] = "Choisir une méthode de qualification";


$lang['qual_infos_1__annee1'] = "Année 1";
$lang['qual_infos_1__annee2'] = "Année 2";
$lang['qual_infos_1__annee3'] = "Année 3";

$lang['qual_infos_2__iron1_titre'] = "Course 1";
$lang['qual_infos_2__iron1_annee'] = "Année";
$lang['qual_infos_2__iron1_chrono'] = "Chrono";

$lang['qual_infos_2__iron2_titre'] = "Course 2";
$lang['qual_infos_2__iron2_annee'] = "Année";
$lang['qual_infos_2__iron2_chrono'] = "Chrono";



$lang['qual_infos_3__titre'] = "Course";
$lang['qual_infos_3__annee'] = "Année";
$lang['qual_infos_3__chrono'] = "Chrono";
$lang['qual_infos_3__position'] = "Position";


$lang['qual_infos_4__titre'] = "Course";
$lang['qual_infos_4__annee'] = "Année";
$lang['qual_infos_4__chrono'] = "Chrono";
$lang['qual_infos_4__position'] = "Position";

$lang['qual_infos_5__titre'] = "Course";
$lang['qual_infos_5__annee'] = "Année";
$lang['qual_infos_5__chrono'] = "Chrono";
$lang['qual_infos_5__position'] = "Position";


$lang['qual_infos_6__titre'] = "Course";
$lang['qual_infos_6__annee'] = "Année";
$lang['qual_infos_6__chrono'] = "Chrono";
$lang['qual_infos_6__position'] = "Position";


$lang['Athlète'] = "Athlète";

$lang['egc_input_athlete_Nageur'] = "Nageur";
$lang['egc_input_athlete_Cyclist'] = "Cyclist";
$lang['egc_input_athlete_Coureur'] = "Coureur à Pieds";


$lang['egc_input_discipline'] = "Discipline";

$lang['egc_titre_etape_2_bis'] = "Equipe";




$lang['Souhaitez payer moins pour votre inscription?'] = "Souhaitez-vous payer moins pour votre inscription?";



$lang['egc_titre_etape_adresse_equipe'] = "Adresse de correspondance pour l'équipe";




$lang['titre_code_promo'] = "Code Promo";


$lang['Total de la commande'] = "Montant total de la commande";










#LE titre des parcours doit être le meme que celui présent dans l'administration 
#Exemple : $lang['4km swim, 181km bike, 43km run'] = "4km natation, 181km vélo, 43km course à pied";

$lang['4km swim, 181km bike, 43km run'] = "4km de natation, 181km de vélo, 43km de course à pied";




$lang['2km swim, 95km bike, 21km run'] = "2km de natation, 95km de vélo, 21km de course à pied";


$lang['Course INDIVIDUELLE'] = "Course INDIVIDUELLE";
$lang['Course de RELAIS'] = "Course de RELAIS";


//RELAY Race, INDIVIDUAL Race


#Page d'accueil
$lang['egc_P_accueil_tableau_evenement'] = "Evénement";
$lang['egc_P_accueil_tableau_entryfee'] = "Droit d'entrée";
$lang['egc_P_accueil_tableau_Quant'] = "Quand : ";
$lang['egc_P_accueil_tableau_Ou'] = "Localisation : ";

$lang['egc_Date_De'] = "Du : ";
$lang['egc_Date_Au'] = " Au : ";

$lang['egc_partagerreseaux'] = "Partager sur les réseaux";

$lang['egc_Bouton_RegisterNow'] = "Inscription";


#Formulaire
$lang['egc_titre_etape_1'] = "Identification";
$lang['egc_titre_etape_2'] = "Méthode de Qualification";
$lang['egc_titre_etape_3'] = "Directives sur l'environnement";


$lang['egc_boutsuivant'] = "Suivant";
$lang['egc_boutprecedent'] = "Précédent";
$lang['egc_boutterminer'] = "Terminer";



#Formulaire

	#ETAPE 1
$lang['egc_input_prenom'] = "Prénom*";
$lang['egc_input_nom'] = "Nom de famille*";
$lang['egc_input_email'] = "Email*";
$lang['egc_input_sexe'] = "Sexe*";
	$lang['egc_input_sexe_Homme'] = "Homme";
	$lang['egc_input_sexe_Femme'] = "Femme";

$lang['egc_input_adresse'] = "Adresse (1)*";
$lang['egc_input_adresse_2'] = "Adresse (2)";
$lang['egc_input_ville'] = "Ville*";
$lang['egc_input_etat'] = "Etat";
$lang['egc_input_cp'] = "Code postal*";
$lang['egc_input_pays'] = "Pays*";
$lang['egc_input_club_equipe'] = "Club / Equipe";
$lang['egc_input_taille_tshirt'] = "Taille T-shirt course et finisseur*";


	#menu droite
$lang['Informations_du_parcours'] = "Informations du parcours";


	#ETAPE 1


$lang['egc_etape2_titre_h2'] = "Directives pour votre participation";
$lang['egc_etape2_texte'] = "Les courses organisées par Evergreen Endurance sont des challenges sportifs difficiles et nous souhaitons que tous nos athlètes soient en mesure de profiter de la course et la finir. Nous conseillons à nos athlètes de demeurer réalistes et conscients de leurs propres capacités athlétiques avant de s’engager. Afin que votre inscription soit acceptée, tout athlète devra avoir atteint au moins un des objectifs suivants:";

$lang['egc_etape2_exi_1'] = "Passé d'athlète d'endurance d'au moins 3 ans";

$lang['egc_etape2_exi_2'] = "Fini 2 triathlons distance demi-iron en 6h15 maximum";
$lang['egc_etape2_exi_3'] = "Accompli 1 triathlon longue distance avec chrono maximum 170% du chrono du vainqueur";
$lang['egc_etape2_exi_4'] = "Accompli 1 autre triathlon iron extreme (Norseman, Swissman, Celtman, Embrunman, Altriman etc.)";
$lang['egc_etape2_exi_5'] = "Avoir terminé toute course de sport d’endurance dans un temps non supérieur a 170% de temps du vainqueur de même groupe d’âge";

$lang['egc_etape2_exi_6'] = "Avoir accompli un triathlon de distance « Iron » en un temps maximum de 14 heures";



$lang['egc_input_confirm_exigences'] = "Je confirme que je satisfais au moins une des exigences énumérées ci-dessus";


$lang['egc_input_nom_course'] = "Nom de la course*";
$lang['egc_input_Chrono'] = "Chrono*";
$lang['egc_input_Position_scratch'] = "Position (scratch)*";
$lang['egc_input_Position_age'] = "Position (groupe d’âge)*";




$lang['egc_input_etape_2_accept'] = "J'accepte les termes et conditions de course d’Evergreen Endurance";


#etape 3 

$lang['egc_etape3_titre_h2'] = "Stratégie Environnementale Evergreen Endurance";

$lang['egc_etape3_texte'] = "Evergreen 118 et 228 ont pour objectif d’être les triathlons de distance iron et demi-iron les plus écologiquement responsables au monde. Evergreen Endurance s’engage à organiser des courses soucieuses de protéger notre environnement et de promouvoir des attitudes durables. Nous demandons à nos athlètes leur aimable collaboration.";





$lang['egc_input_depart'] = "D‘où voyagerez vous pour vous rendre à Chamonix?*";


$lang['egc_etape3_texte_transport'] = "
Mode(s) de transport utilisé ?";

$lang['egc_input_etape_3_transmport_avion'] = "Avion";
$lang['egc_input_etape_3_transmport_train'] = "Train";
$lang['egc_input_etape_3_transmport_bus'] = "Bus";
$lang['egc_input_etape_3_transmport_voiture'] = "Voiture";
$lang['egc_input_de'] = "De*";
$lang['egc_input_a'] = "A*";

$lang['egc_input_transport_autre'] = "Autre";

$lang['egc_input_co_voiturage'] = "Si vous voyagez intra Europe en voiture, ferez vous du co-voiturage? *";

$lang['egc_input_Oui'] = "Oui";
$lang['egc_input_Non'] = "Non";



$lang['egc_input_supporters'] = "Combien de spectateurs se joindront a vous pour vous supporter?";
$lang['egc_input_supporters_deplacement'] = "Comment voyageront-ils?";

$lang['egc_input_navette'] = "Souhaiterez vous utiliser notre service de navette afin d’assurer vos transferts depuis Chamonix à T1 (Montriond ~ 75 km) la veille de la course et le jour de la course (10 euros)?*";
 
$lang['egc_etape3_titre_levee_de_fond'] = "Levé de fonds";

$lang['egc_etape3_texte_levee_de_fond'] = "Evergreen Endurance s’engage à contribuer à la préservation de notre environnement. Afin d'atteindre notre objectif de contribution, Evergreen Endurance, en tant qu'organisateur, réallouera une partie des bénéfices de l'événement à la préservation de l'environnement. Nous demandons également à nos athlètes de nous aider et de contribuer. Mobilisez vos réseaux personnels et professionnels respectifs pour passer le mot et nous aider à lever des fonds. Le montant d'argent que vous lèverez se traduira par un remboursement partiel ou total de vos frais d'inscription et sera réaffecté à la préservation de l'environnement d'une manière totalement transparente. Nous vous remercions d'avance pour votre contribution.<br> Engagez-vous et faites la différence.";



$lang['egc_input_choix_levee_de_fond'] = "Merci de nous dire quelle somme vous pensez pourvoir lever*";



$lang['egc_input_levee__choix'] = "";
$lang['egc_input_levee__0'] = "Je choisis de ne pas contribuer à des levées de fonds";
$lang['egc_input_levee__1'] = "Je ferai de mon mieux pour lever un total minimum de 500€";
$lang['egc_input_levee__2'] = "Je ferai de mon mieux pour lever un total minimum de 1000€";
$lang['egc_input_levee__3'] = "Je ferai de mon mieux pour lever un total minimum de 2000€";
$lang['egc_input_levee__4'] = "Je ferai de mon mieux pour lever un total minimum de 3000€";
$lang['egc_input_levee__5'] = "Je ferai de mon mieux pour lever un total minimum de 4000€";
$lang['egc_input_levee__6'] = "Je ferai de mon mieux pour lever un total minimum de 5000€";
$lang['egc_input_levee__7'] = "Je ferai de mon mieux pour lever un total minimum de 7500€";


$lang['egc_input_levee__0__TEXTE'] = "Nous savons bien que l’activité de collecte de fonds peut être une tâche difficile. Sachez que si vous parvenez finalement à lever des fonds, vous resterez éligible à un remboursement partiel ou total de vos frais d'entrée en fonction du montant que vous aurez finalement réussi à lever. N’hésitez pas à contacter les organisateurs pour plus d'informations.";
$lang['egc_input_levee__1__TEXTE'] = "Vous aurez droit à une réduction de <strong>10%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__2__TEXTE'] = "Vous aurez droit à une réduction de <strong>20%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__3__TEXTE'] = "Vous aurez droit à une réduction de <strong>30%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__4__TEXTE'] = "Vous aurez droit à une réduction de <strong>40%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__5__TEXTE'] = "Vous aurez droit à une réduction de <strong>50%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__6__TEXTE'] = "Vous aurez droit à une réduction de <strong>75%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";
$lang['egc_input_levee__7__TEXTE'] = "Vous aurez droit à une réduction de <strong>100%</strong> sur vos frais d'inscription. Le montant correspondant vous sera remboursé une fois que vous aurez atteint votre objectif de levée de fonds, soit sous la forme d’un remboursement soit la forme d’une place garantie pour l’année suivante ainsi que d’un crédit sur le droit d'entrée de l'année suivante";




$lang['egc_input_etape_3_accept_final'] = "Termes et conditions Evergreen Endurance 
  J’accepte les Termes et Conditions *";












#Validation

$lang['JQ_required'] = "Ce champ est requis";
$lang['JQ_remote'] = "Veuillez remplir ce champ pour continuer.";
$lang['JQ_email'] = "Veuillez entrer une adresse email valide.";
$lang['JQ_url'] = "Veuillez entrer une URL valide.";
$lang['JQ_date'] = "Veuillez entrer une date valide.";
$lang['JQ_dateISO'] = "Veuillez entrer une date valide (ISO).";
$lang['JQ_number'] = "Veuillez entrer un nombre valide.";
$lang['JQ_digits'] = "Veuillez entrer (seulement) une valeur numÃ©rique.";
$lang['JQ_creditcard'] = "Veuillez entrer un numÃ©ro de carte de crÃ©dit valide.";
$lang['JQ_equalTo'] = "Veuillez entrer une nouvelle fois la mÃªme valeur.";
$lang['JQ_accept'] = "Veuillez entrer une valeur avec une extension valide.";

$lang['JQ_maxlength'] = "Veuillez ne pas entrer plus de {0} caractÃ¨res.";
$lang['JQ_minlength'] = "Veuillez entrer au moins {0} caractÃ¨res.";
$lang['JQ_rangelength'] = "Veuillez entrer entre {0} et {1} caractÃ¨res.";
$lang['JQ_range'] = "Veuillez entrer une valeur entre {0} et {1}.";
$lang['JQ_max'] = "Veuillez entrer une valeur infÃ©rieure ou Ã©gale Ã  {0}.";
$lang['JQ_min'] = "Veuillez entrer une valeur supÃ©rieure ou Ã©gale Ã  {0}.";




#Dates 
$lang['Janvier'] 		= "Janvier";
$lang['Fevrier'] 		= "Fevrier";
$lang['Mars'] 			= "Mars";
$lang['Avril'] 			= "Avril";
$lang['Mai'] 			= "Mai";
$lang['Juin'] 			= "Juin";
$lang['Juillet']		= "Juillet";
$lang['Aout'] 			= "Août";
$lang['Septembre'] 		= "Septembre";
$lang['Octobre'] 		= "Octobre";
$lang['Novembre'] 		= "Novembre";
$lang['Decembre'] 		= "Decembre";