<?php
/* Fichier de traduction du module de carte interactive et d'itinéraire */




$lang['carte_titre_accueil'] = "Carte interactive" ;
$lang['carte_titre_itineraire'] = "Itinéraire" ;



$lang['carte_message_accueil'] = "Retrouvez toutes les informations proche de notre commune." ;
$lang['carte_message_accueil_menu_cases'] = "Cocher, décocher les éléments que vous souhaitez afficher." ;

$lang['carte_message_itineraire'] = "Générer un itinéraire." ;
$lang['carte_message_itineraire_evite_autoroutes'] = "Votre itinéraire évite les autoroutes." ;
$lang['carte_message_itineraire_evite_peages'] = "Votre itinéraire évite les péages." ;




$lang['carte_input_pistes_cyclabes'] = "Pistes cyclables" ;
$lang['carte_input_photos'] = "Photos" ;
$lang['carte_id_users_photos'] = 5413900 ;
$lang['carte_lieu_de_destination'] = "25250 Soye" ;




$lang['carte_label_adresse'] = "Adresse de départ" ;
$lang['carte_label_autoroutes'] = "Autoroutes" ;
$lang['carte_label_peages'] = "Peages" ;



$lang['carte_label_eviter_les_autoroutes'] = "Eviter les autoroutes" ;
$lang['carte_label_eviter_les_peages'] = "Eviter les péages" ;


$lang['carte_placeholder_adresse'] = "adresse cp ville" ;


$lang['carte_label_photos'] = "Afficher les photos" ;
$lang['carte_label_pistes'] = "Afficher les pistes cyclables (lignes vertes)" ;




$lang['carte_bouton_actualiser_carte'] = "Actualiser la carte" ;
$lang['carte_bouton_generer_itineraire'] = "Générer l'itinéraire" ;
$lang['carte_bouton_imprimer_itineraire'] = "Imprimer" ;
$lang['carte_bouton_changer_itineraire'] = "Changer l'itinéraire" ;








/*


$lang['blog_titre_accueil'] = "Articles" ;
$lang['blog_titre_categories'] = "Catégories" ;
$lang['blog_titre_recherche'] = "Recherche" ;
$lang['blog_titre_auteur'] = "Auteur" ;

$lang['blog_titre_breadcrumb_categorie'] = "Catégorie" ;
$lang['blog_titre_breadcrumb_auteur'] = "Auteur" ;
$lang['blog_titre_breadcrumb_recherche'] = "Recherche" ;


$lang['blog_message_aucun_article'] = "Il n'y a aucun article" ;
$lang['blog_message_aucun_article_correspondant'] = "Il n'y a aucun article à cette adresse" ;
$lang['blog_message_aucun_article_correspondant_recherche'] = "Il n'y a aucun article qui corresponde à votre recherche" ;
$lang['blog_message_resultat_recherche'] = "Il y a <strong>%1\$s</strong> article(s) correspondant(s) à votre recherche <strong>\"%2\$s\"</strong> " ;
$lang['blog_message_resultat_categorie'] = "Ci dessous les articles correspondants à la catégorie <strong>%1\$s</strong>" ;
$lang['blog_message_resultat_auteur'] = "Ci dessous les articles publiés par <strong>%1\$s</strong>" ;
 


$lang['blog_article_date_post'] = "Ecrit le" ;
$lang['blog_article_lien'] = "Lire la suite" ;
$lang['blog_article_retour_aux_articles'] = "Retour aux articles" ;




#page article

$lang['blog_article_titre_photos'] = "Les photos" ;
$lang['blog_article_titre_documents'] = "Les documents" ;
$lang['blog_article_bouton_telecharger_documents'] = "Télécharger" ;


#page article Commentaires



$lang['blog_article_commentaire_date_post'] = "Posté le : " ;



$lang['blog_article_commentaire_tooltip_modif_message'] = "Modifier le message" ;
$lang['blog_article_commentaire_tooltip_supprimer_message'] = "Supprimer le message" ;


$lang['blog_article_bouton_ajouter_commentaires'] = "Ajouter un commentaire" ;
$lang['blog_article_bouton_se_connecter_ajouter_commentaires'] = "Se connecter pour ajouter un commentaire" ;
$lang['blog_article_message_se_connecter_ajouter_commentaires'] = "Pour ajouter un commentaire vous devez être connecter." ;

$lang['blog_article_message_commentaires_bien_ajoute'] = "Votre commentaire a bien été ajouté." ;
$lang['blog_article_message_commentaires_bien_modifier'] = "Votre commentaire a bien été modifié." ;
$lang['blog_article_message_commentaires_aucun_commentaire'] = "Aucun commentaire, soyez le premier à poster un commentaire !" ;


$lang['blog_article_message_commentaires_titre_modal_modifier'] = "Modifier le commentaire" ;
$lang['blog_article_message_commentaires_bouton_modal_modifier'] = "Valider les modifications" ;

$lang['blog_article_input_label_message'] = "Votre message" ;
$lang['blog_article_input_label_annuler_message'] = "Annuler" ;
$lang['blog_article_input_label_publier_message'] = "Ajouter mon commentaire" ;

*/