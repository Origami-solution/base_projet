<?php


$lang['identifiant_unique']		= _("Un compte existe déjà avec cet %s");
$lang['captcha']		= _("Le code anti-robot n'est pas identique");
$lang['code_unique']		= _("Ce %s existe déjà dans notre base de donnée");
$lang['identifiant_existe']		= _("Ce %s n'éxiste pas dans notre base de donnée");
$lang['is_unique']		= _("Un compte existe déjà avec cet %s");



$lang['required']			= _("Le champ %s est requis.");
$lang['isset']				= _("Le champ %s doit avoir une valeur.");
$lang['valid_email']		= _("Le champ %s doit contenir une adresse email valide.");
$lang['valid_emails']		= _("Le champ %s ne peut contenir que des adresses email valides.");
$lang['valid_url']			= _("Le champ %s doit contenir une URL valide.");
$lang['valid_ip']			= _("Le champ %s doit contenir une IP valide.");
$lang['min_length']			= _("Le champ %s doit contenir au moins %s caract&egrave;res.");
$lang['max_length']			= _("Le champ %s ne peut contenir plus de %s caract&egrave;res.");
$lang['exact_length']		= _("Le champ %s doit contenir exactement %s caract&egrave;res.");
$lang['alpha']				= _("Le champ %s ne peut contenir que des caract&egrave;res alphab&eacute;tiques.");
$lang['alpha_numeric']		= _("Le champ %s ne peut contenir que des caract&egrave;res alphanum&eacute;riques.");
$lang['alpha_dash']			= _("Le champ %s ne peut contenir que des caract&egrave;res alphanum&eacute;riques, des tirets bas (underscore) et des traits d'union.");
$lang['numeric']			= _("Le champ %s doit contenir un nombre (caract&egrave;res num&eacute;riques).");
$lang['is_numeric']			= _("Le champ %s ne peut contenir que de signes du type nombre.");
$lang['integer']			= _("Le champ %s doit contenir un integer.");
$lang['regex_match']		= _("Le champ %s field n'utilise pas le bon format.");
$lang['matches']			= _("Le champ %s doit correspondre au champ %s.");
$lang['is_natural']			= _("Le champ %s ne peut contenir que des nombres positifs.");
$lang['is_natural_no_zero']	= _("Le champ %s ne peut contenir que des nombres plus grand que z&eacute;.");
$lang['decimal']			= _("Le champ %s doit contenir un nombre d&eacute;cimal.");
$lang['less_than']			= _("Le champ %s doit contenir un nombre inf&eacute;rieur &agrave; %s.");
$lang['greater_than']		= _("Le champ %s doit contenir un nombre sup&eacute;rieur &agrave; %s.");



/* End of file form_validation_lang.php */
/* Location: ./system/language/french/form_validation_lang.php */