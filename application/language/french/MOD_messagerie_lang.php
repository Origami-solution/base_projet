<?php
#MODULE     MESSAGERIE


$lang['messagerie_titre'] = "Ma messagerie" ;

$lang['messagerie_titre_boite_reception'] = "Boîte de réception" ;
$lang['messagerie_titre_messages_envoyes'] = "Messages envoyés" ;
$lang['messagerie_titre_messages_supprimes'] = "Messages supprimés" ;
$lang['messagerie_titre_config'] = "Configuration de la messagerie" ;
$lang['messagerie_titre_nouveau_message'] = "Nouveau message" ;
$lang['messagerie_titre_message'] = "Message" ;
$lang['messagerie_titre_repondre_message'] = "Répondre au message" ;



#boutons
$lang['messagerie_bouton_nouveau_message'] = "Ecrire un message" ;
$lang['messagerie_bouton_suppimer_messages_selectionnes'] = "Supprimer les messages selectionnés" ;
$lang['messagerie_bouton_alertes_emails'] = "Alertes emails" ;
$lang['messagerie_bouton_close'] = "Fermer" ;
$lang['messagerie_bouton_retour_boite_reception'] = "Retour à la boîte de réception" ;
$lang['messagerie_bouton_retour_messages_envoyes'] = "Retour aux messages envoyés" ;
$lang['messagerie_bouton_activer_email'] = "Activer" ;
$lang['messagerie_bouton_desactiver_email'] = "Désactiver" ;
$lang['messagerie_bouton_envoyer_email'] = "Envoyer le message" ;
$lang['messagerie_bouton_repondre'] = "Répondre" ;
$lang['messagerie_bouton_supprimer'] = "Supprimer" ;
$lang['messagerie_bouton_annuler_supprimer'] = "Annuler la suppression" ;
$lang['messagerie_bouton_signaler'] = "Signaler le message" ;


#tooltips
$lang['messagerie_tooltips_configuration'] = "Configuration de la messagerie" ;
$lang['messagerie_tooltips_configuration_activer_email'] = "Activer les emails, vous receverez des emails pour vous informer de nouveaux messages" ;
$lang['messagerie_tooltips_configuration_desactiver_email'] = "Désactiver les emails, vous ne receverez plus les emails d'alerte" ;
$lang['messagerie_tooltips_signaler_message'] = "Si le message ne respecte pas la charte, vous pouvez nous le signaler afin que nous puissions faire le nécessaire" ;







#tableau
$lang['messagerie_table_date'] = "Date" ;
$lang['messagerie_table_expediteur'] = "Expediteur" ;
$lang['messagerie_table_destinataire'] = "Destinataire" ;
$lang['messagerie_table_sujet'] = "Sujet" ;


$lang['messagerie_table_non_lu'] = "Non lu" ;
$lang['messagerie_table_lu'] = "Lu" ;



#alert

$lang['messagerie_message_alert_warning_aucun_message'] = "Aucun message reçu" ;
$lang['messagerie_message_alert_warning_aucun_message_envoyes'] = "Aucun message envoyé" ;
$lang['messagerie_message_alert_warning_aucun_message_corbeille'] = "Aucun message dans la corbeille" ;
$lang['messagerie_message_alert_warning_message_introuvable'] = "Message introuvable" ;



$lang['messagerie_message_alert_succes_messages_suppr'] = "Les messages ont bien été supprimer" ;
$lang['messagerie_message_alert_succes_message_suppr'] = "Le message à bien été supprimé" ;
$lang['messagerie_message_alert_succes_message_deplacer_boite'] = "Le message à bien été déplacé dans la boîte de réception" ;
$lang['messagerie_message_alert_succes_message_signaler'] = "Le message à bien été signalé, nous traiterons votre demande dans les plus brefs délais" ;
$lang['messagerie_message_alert_succes_message_dela_signaler'] = "Le message à déjà été signalé, celui-ci sera ou à été traité par nos services" ;
$lang['messagerie_message_alert_succes_message_envoyer'] = "Le message à bien été envoyé" ;




$lang['messagerie_message_alert_js_supprimer_message'] = "Supprimer le message ?" ;
$lang['messagerie_message_alert_js_signaler_message'] = "Signaler le message ?" ;
$lang['messagerie_message_alert_js_deplacer_message_boite_reception'] = "Déplacer le message dans la boîte de réception ?" ;




#email d'alert que l'utilisateur recoi pour lui informer qu'il a un nouveau message
$lang['messagerie_email_nouveau_message_A'] = "Expéditeur du message : %1\$s" ;
$lang['messagerie_email_nouveau_message_B'] = "Pour afficher le message et y répondre, veuillez" ;
$lang['messagerie_email_nouveau_message_C'] = "cliquez ici" ;
$lang['messagerie_email_nouveau_message_D'] = "Message automatique envoyé depuis le site %1\$s, merci de NE PAS REPONDRE A CE MAIL" ;
$lang['messagerie_email_nouveau_message_Sujet'] = "Nouveau message de %1\$s, %2\$s" ;



#email de copie d'envoi pour informer l'expediteur que sont message est bien parti
$lang['messagerie_email_confirmation_message_A'] = "Destinataire du message : %1\$s" ;
$lang['messagerie_email_confirmation_message_B'] = "Message automatique envoyé depuis le site %1\$s" ;
$lang['messagerie_email_confirmation_message_Sujet'] = "Confirmation du message %1\$s" ;






#formulaire

$lang['messagerie_input_Message'] = "Message" ;
$lang['messagerie_input_Sujet'] = "Sujet" ;
$lang['messagerie_input_Destinataire'] = "Destinataire" ;

$lang['messagerie_label_Destinataire'] = "Choisir le destinataire : " ;
$lang['messagerie_label_Expediteur'] = "Expediteur" ;



$lang['messagerie_label_Sujet'] = "Sujet" ;
$lang['messagerie_placeholder_Sujet'] = "Sujet" ;


$lang['messagerie_label_Message'] = "Message" ;

$lang['messagerie_label_copie_email'] = "Recevoir une copie du message par email" ;
$lang['messagerie_label_message_precedent'] = "Message précédent du" ;




#textes

$lang['messagerie_texte_infos_config_alertes'] = "Les alertes vous permettent d'être informé par E-mail lorsque vous recevez un nouveau message." ;
$lang['messagerie_texte_infos_envoyer_le'] = "Envoyé le" ;




/*
$lang['phototheque_titre_breadcrumb_album'] = "Album" ;
$lang['phototheque_titre_breadcrumb_recherche'] = "Recherche" ;




$lang['phototheque_titre_accueil_galere_photo'] = "Galerie photo" ;
$lang['phototheque_titre_recherche'] = "Rechercher" ;
$lang['phototheque_titre_panel_albums_recements_ajoutes'] = "Les albums récements ajoutés" ;



$lang['phototheque_message_accueil_galerie'] = "Retrouvez les évènements de notre commune immortalisés dans la photothèque." ;
$lang['phototheque_message_recherche'] = "Votre recherche." ;
$lang['phototheque_message_recherche_resultat'] = "Il n'y a pas d'album correspondant à votre recherche." ;


$lang['phototheque_message_accueil_galerie_aucun_album'] = "Il n'y a pas d'album pour le moment." ;
$lang['phototheque_message_accueil_galerie_aucune_categorie'] = "Il n'y a pas de catégorie." ;


$lang['phototheque_message_mentions_legales'] = "%1\$s Informations légales et règlement %2\$s des photos publiés sur ce site" ;
$lang['phototheque_message_contact'] = "Vous possèdez des photos qui pourraient être présentes dans notre photothèque, veuillez %1\$s nous contacter %2\$s" ;


$lang['phototheque_texte_detail_nbr_photo'] = "photos" ;
*/
















/*
$lang['blog_titre_categories'] = "Catégories" ;
$lang['blog_titre_recherche'] = "Recherche" ;
$lang['blog_titre_auteur'] = "Auteur" ;

$lang['blog_titre_breadcrumb_categorie'] = "Catégorie" ;
$lang['blog_titre_breadcrumb_auteur'] = "Auteur" ;
$lang['blog_titre_breadcrumb_recherche'] = "Recherche" ;



$lang['blog_message_aucun_article_correspondant'] = "Il n'y a aucun article à cette adresse" ;
$lang['blog_message_aucun_article_correspondant_recherche'] = "Il n'y a aucun article qui corresponde à votre recherche" ;
$lang['blog_message_resultat_recherche'] = "Il y a <strong>%1\$s</strong> article(s) correspondant(s) à votre recherche <strong>\"%2\$s\"</strong> " ;
$lang['blog_message_resultat_categorie'] = "Ci dessous les articles correspondants à la catégorie <strong>%1\$s</strong>" ;
$lang['blog_message_resultat_auteur'] = "Ci dessous les articles publiés par <strong>%1\$s</strong>" ;
 


$lang['blog_article_date_post'] = "Ecrit le" ;
$lang['blog_article_lien'] = "Lire la suite" ;
$lang['blog_article_retour_aux_articles'] = "Retour aux articles" ;




#page article

$lang['blog_article_titre_photos'] = "Les photos" ;
$lang['blog_article_titre_documents'] = "Les documents" ;
$lang['blog_article_bouton_telecharger_documents'] = "Télécharger" ;


#page article Commentaires



$lang['blog_article_commentaire_date_post'] = "Posté le : " ;



$lang['blog_article_commentaire_tooltip_modif_message'] = "Modifier le message" ;
$lang['blog_article_commentaire_tooltip_supprimer_message'] = "Supprimer le message" ;


$lang['blog_article_bouton_ajouter_commentaires'] = "Ajouter un commentaire" ;
$lang['blog_article_bouton_se_connecter_ajouter_commentaires'] = "Se connecter pour ajouter un commentaire" ;
$lang['blog_article_message_se_connecter_ajouter_commentaires'] = "Pour ajouter un commentaire vous devez être connecter." ;

$lang['blog_article_message_commentaires_bien_ajoute'] = "Votre commentaire a bien été ajouté." ;
$lang['blog_article_message_commentaires_bien_modifier'] = "Votre commentaire a bien été modifié." ;
$lang['blog_article_message_commentaires_aucun_commentaire'] = "Aucun commentaire, soyez le premier à poster un commentaire !" ;


$lang['blog_article_message_commentaires_titre_modal_modifier'] = "Modifier le commentaire" ;
$lang['blog_article_message_commentaires_bouton_modal_modifier'] = "Valider les modifications" ;

$lang['blog_article_input_label_message'] = "Votre message" ;
$lang['blog_article_input_label_annuler_message'] = "Annuler" ;
$lang['blog_article_input_label_publier_message'] = "Ajouter mon commentaire" ;

*/