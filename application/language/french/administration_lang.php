<?php
/* Fichier de traduction de l'administration */


# Général 

$lang['admin_titre_administration'] = "Administration";

# page : de connexion

$lang['admin_login'] = "Identifiant";
$lang['admin_pass'] = "Mot de passe";
$lang['admin_button_connexion'] = "Connexion";
$lang['admin_pass_perdu'] = "Mot de passe perdu ?";


# page : mot de passe perdu

$lang['admin_se_reconnecter'] = "Se reconnecter";
$lang['admin_reset_password'] = "Réinitialiser le mot de passe";
$lang['admin_message_reset_password'] = "Vous avez perdu votre mot de passe ? nous pouvons vous en générer un nouveau, veuillez inscrire votre email";


#menu TOP

$lang['admin_mon_profil'] = "Mon compte";
$lang['admin_fullscreen'] = "Plein écran";
$lang['admin_deconnexion'] = "Se déconnecter";
$lang['admin_verouiller'] = "Vérouiller";


#Menu de gauche

$lang['admin_menu_tableau_de_bord'] = "Tableau de bord";
$lang['admin_menu_statistiques'] = "Statistiques";


	#LES UTILISATEURS
$lang['admin_menu_utilisateurs'] = "Utilisateurs";
$lang['admin_menu_utilisateurs_liste'] = "Liste des utilisateurs";
$lang['admin_menu_utilisateurs_liste_attente'] = "Liste des utilisateurs en attente";
$lang['admin_menu_utilisateurs_liste_moderateurs'] = "Liste des modérateurs";
$lang['admin_menu_utilisateurs_liste_administrateurs'] = "Liste des administrateurs";
$lang['admin_menu_utilisateurs_groupes'] = "Les groupes";
$lang['admin_menu_utilisateurs_groupes_titre'] = "La liste des groupes d'utilisateurs";


$lang['admin_well_utilisateurs'] = "Ci-dessous la liste des utilisateurs";
$lang['admin_well_utilisateurs_en_attente'] = "Ci-dessous la liste des utilisateurs en attente d'activation, actuellement ils ne peuvent pas se connecter sur le site.";
$lang['admin_well_moderateurs'] = "Ci-dessous la liste des modérateurs, les modérateurs ont la possibilité de...";
$lang['admin_well_adminstrateurs'] = "Ci-dessous la liste des administrateurs qui ont plein pouvoir sur le site.";



	#LES MESSAGES
$lang['admin_menu_messagerie'] = "Les signalements";
$lang['admin_well_messagerie'] = "Les messages signalés comme indésirables";
$lang['admin_well_messagerie_action_signalement'] = "Voici les actions pour le message signalé";




	#LE BLOG
$lang['admin_menu_blog'] = "Blog / Actualité";
$lang['admin_menu_blog_liste_articles'] = "Les articles";
$lang['admin_menu_blog_liste_categories'] = "Les catégories";
$lang['admin_menu_blog_liste_en_attente'] = "Articles en attente";
$lang['admin_menu_blog_commentaires_liste_en_attente'] = "Commentaires en attente";



	#LE PORTFOLIO
$lang['admin_menu_portfolio'] = "Portfolio" ;

$lang['portfolio_titre_accueil'] = "Portfolio" ;
$lang['admin_menu_portfolio_liste_articles'] = "Portfolio articles" ;
$lang['admin_menu_portfolio_liste_categories'] = "Catégories portfolio" ;
$lang['admin_menu_portfolio_liste_groupes'] = "Groupes portfolio" ;
$lang['admin_menu_portfolio_liste_photos'] = "Groupes portfolio" ;
$lang['admin_menu_portfolio_statistiques'] = "Statistiques portfolio" ;










	#LA BOUTIQUE
$lang['admin_menu_boutique'] = "Boutique";
$lang['admin_menu_boutique_produits'] = "Les produits";
$lang['admin_menu_boutique_categories'] = "Les catégories de produits";





	#LE CAROUSSEL
$lang['admin_menu_caroussel'] = "Caroussel accueil";



//$lang['admin_menu_blog_liste'] = "Les groupes";
//$lang['admin_menu_blog_liste'] = "La liste des groupes d'utilisateurs";



$lang['admin_menu_blog_bouton_retour_articles'] = "Retour aux articles";



	#LA PHOTOTHEQUE
$lang['admin_menu_phototheque'] = "Phototheque";

$lang['admin_menu_phototheque_liste'] = "Les albums";
$lang['admin_menu_phototheque_liste_categories'] = "Les catégories des albums";
$lang['admin_menu_phototheque_liste_en_attente'] = "Les albums en attente";




	#LES DOCUMENTS
$lang['admin_menu_documents'] = "Documents";

$lang['admin_menu_documents_liste'] = "Les fiches";
$lang['admin_menu_documents_liste_categories'] = "Les catégories des fiches";


$lang['admin_menu_documents_bouton_retour_fiches'] = "Retour aux fiches";





	#CONTACT
$lang['admin_menu_contact'] = "Contact";

	#CONFIG
$lang['admin_menu_config'] = "config";


$lang['admin_menu_bloc_page'] = "Pages et blocs";
$lang['admin_well_bloc_page_choix'] = "Veuillez choisir la page concernée";
$lang['admin_well_bloc_page_retour_choix'] = "Retour aux choix des pages %1\$s";


	#LES ASSOCIATION
$lang['admin_menu_association'] = "Associations";

$lang['admin_menu_association_liste'] = "Les Associations";
$lang['admin_menu_association_liste_categories'] = "Les catégories des Associations";

	


	#LES NEWSLETTER
$lang['admin_menu_newsletter'] = "Lettre d'information";

$lang['admin_menu_newsletter_liste'] = "Lettre d'information";
$lang['admin_menu_newsletter_liste_emails'] = "les emails inscrits";
$lang['admin_menu_newsletter_liste_themes'] = "les themes";






	#LES ENTREPRISES
$lang['admin_menu_entreprises'] = "Entreprises";
$lang['admin_menu_entreprises_liste'] = "Les entreprises";



	#CARTE
$lang['admin_menu_carte'] = "Carte interactive";

$lang['admin_menu_carte_liste'] = "Les points de la carte";
$lang['admin_menu_carte_liste_categories'] = "Les catégories de points";
$lang['admin_menu_carte_liste_config'] = "Configuration";



	#Bloc d'information de la page d'acceuil
$lang['admin_menu_bloc_informations'] = "Bloc d'informations";



	#Bloc d'information de la page d'acceuil
$lang['admin_menu_bloc_partenaires'] = "Les partenaires";






# LES UTILISATEURS 


//$lang['admin_users_titre'] = "Les utilisateurs";
