<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 	GESTION DES UTILISATEURS DU SITE
 *  
 *
 *
 * @package		CodeIgniter 2.2.1
 * @author 		Guigon Gilles contact@origami-solution.com
 * @version     1.0.0
 *
 **/



class Emails_lib
{
  protected 	$ci;

	public function __construct()
	{
		
		$this->__Verif_table();
		$this->__verif();
		

	}





	public function __verif()
		{
				$CI =& get_instance(); 


				
				$CI->config->load('emails_config');
				$CI->load->helper('emails');
				$CI->load->model('emails_model');
				$CI->load->library('email');
				$CI->lang->load('email_lib','french');
				
		}



/**
 * LISTE DES EMAILS
 */



/**
 * Email envoyé lors de l'inscriptino de l'utilisateur
 * @date   2015-05-16
 * @auteur Gilles     Guigon      -             contact@origami-solution.com
 * @param  [type]     $data_user [Liste des infois de l'inscription]
 */
	public function Mail__Inscription($data_user)
	{
		
		//var_dump($data_user);

	  		$CI =& get_instance(); 
			$this->config_emails();

			ob_start();
				

#Template Header
$CI->load->view($CI->config->item("mod_email_url_template_default_header"));


 echo sprintf(lang('email_text_inscription'),$data_user["prenom"],lang("Titre du site"),$data_user["email"],decrypt($data_user["pass"])); 

#Template Footer
$CI->load->view($CI->config->item("mod_email_url_template_default_footer"));


	$message = ob_get_contents(); 
	ob_end_clean(); 


	$sujet = sprintf(lang('email_sujet_inscription'),lang("Titre du site"));


			$CI->email->from(email_administrateur());
			$CI->email->to($data_user["email"]);
			//$CI->email->bcc(email_administrateur_bcc());
			$CI->email->subject($sujet);
			$CI->email->message($message);
			$CI->email->send();


	
$module 	= 	"Inscription" ;
$infos 		=	"Inscription au site";

$this->insert_log_mail($data_user["email"],email_administrateur(),$message,$sujet,$module,$infos);

//($email_destinataire,$email_expediteur,$message,$sujet,$module,)

	}






/**
 * Email envoyé lors de la récuperation du mot de passe
 * @date   2015-05-16
 * @auteur Gilles     Guigon      -             contact@origami-solution.com
 * @param  [type]     $data_user [Liste des infois de l'inscription]
 */
	public function Mail__Recup_mot_de_passe($data_user)
	{
		foreach ($data_user as $key => $r) {}
		//var_dump($data_user);

	  		$CI =& get_instance(); 
			$this->config_emails();

			ob_start();
				

#Template Header
$CI->load->view($CI->config->item("mod_email_url_template_default_header"));


 echo sprintf(lang('email_text_recup_mot_passe'),$r -> prenom,lang("Titre du site"),$r -> email,decrypt($r -> pass)); 

#Template Footer
$CI->load->view($CI->config->item("mod_email_url_template_default_footer"));


	$message = ob_get_contents(); 
	ob_end_clean(); 


	$sujet = sprintf(lang('email_sujet_recup_mot_passe'),lang("Titre du site"));


			$CI->email->from(email_administrateur());
			$CI->email->to($r -> email);
			//$CI->email->bcc(email_administrateur_bcc());
			$CI->email->subject($sujet);
			$CI->email->message($message);
			$CI->email->send();


	
$module 	= 	"Récupération du mot de passe" ;
$infos 		=	"Récupération du mot de passe";

$this->insert_log_mail($r -> email,email_administrateur(),$message,$sujet,$module,$infos);

//($email_destinataire,$email_expediteur,$message,$sujet,$module,)

	}













	#	Envoi mail a l'UTILISATEUR pour lui informer que son inscription a été validé
	#	
	#	
	public function Envoi_mail_User($id_user,$id_theme)
	{
		$CI =& get_instance(); 

				$infos_user = $CI->users_model->get_info_compte($id_user);
				foreach ($infos_user as $key => $user) {}
				
				$infos_email_theme = $this->get_theme_id($id_theme);
				foreach ($infos_email_theme as $key => $theme) {}


				$this->config_emails();

				ob_start();
				?>

				<html>
				<head>
				</head>
				<body>
	
<?php 			$contenu = $theme -> contenu ; 


				$contenu = str_replace("%PSEUDO%", $user -> pseudo, $contenu);
				$contenu = str_replace("%NOM%", $user -> nom, $contenu);
				$contenu = str_replace("%PRENOM%", $user -> prenom, $contenu);
				$contenu = str_replace("%EMAIL%", $user -> email, $contenu);
				


				
				$CI->load->helper('cryptage');
				$decrypted_string = decrypt($user -> pass);

				$contenu = str_replace("%PASS%", $decrypted_string, $contenu);
				 
				
				echo $contenu;
?>

				</body>
				</html>
	<?php
	$message = ob_get_contents(); 
	ob_end_clean(); 

			$CI->email->from($this->email_administrateur());
			$CI->email->to($user -> email);
			//$CI->email->bcc(email_administrateur_bcc());
			$CI->email->subject($theme -> sujet);
			$CI->email->message($message);
			$CI->email->send();

	$this->insert_log_mail($user -> email,$this->email_administrateur(),$message,$theme -> sujet,"","");
	}




















/**
 * Les infos de configuration des emails
 * @date   2015-05-16
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
public function config_emails()
	{

				$CI =& get_instance(); 

                $CI->load->library('email');
				$config['charset'] = 'utf-8';
				$config['mailtype'] = 'html';
				$config['protocol'] = 	$CI->config->item('email_administrateur_type');
				$config['smtp_host'] = 	$CI->config->item('email_administrateur_host');
				$config['smtp_user'] = 	$CI->config->item('email_administrateur_user');
				$config['smtp_pass'] = 	$CI->config->item('email_administrateur_pass');
				$config['smtp_port'] = 	$CI->config->item('email_administrateur_port');
				$CI->email->initialize($config);
				$CI->email->clear(TRUE); //Effacement de la piece jointe !!

	}





/**
 * Création de la table de log mail
 * @date   2015-05-16
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
	public function __Verif_table()
	{
  		$CI =& get_instance(); 
		if (!$CI->db->table_exists('mails_log'))
		{

                $fields_mails_log = array(
                	'id' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'null' => FALSE,'auto_increment' => TRUE),
                    'date_envoi' => array('type' => 'DATETIME','null' => FALSE),
                  	'expediteur' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
            		'destinataires' => array('type' => 'TEXT','null' => TRUE,'default' => ''),
                    'message' => array('type' => 'TEXT','null' => TRUE,'default' => ''),
                    'module' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'detail' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'sujet' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                );
				
		$CI->load->dbforge();
        $CI->dbforge->add_field($fields_mails_log);
        $CI->dbforge->add_key('id', TRUE);
        $CI->dbforge->create_table('mails_log', TRUE);
        log_message('debug', 'Log_mails > Création de la BDD mails_log');
		}

	}



	#Ajout du mail envoyer dans les logs
	#
	public function insert_log_mail($email_destinataire,$email_expediteur,$message,$sujet,$module,$detail)
	{
		$CI =& get_instance();
		$data_mail = array(

					'date_envoi' 		=> 		date("Y-m-d H:i:s"),
					'expediteur'		=>		$email_expediteur,
					'destinataires'		=>		$email_destinataire,
					'message'			=>		$message,
					'module'			=>		$module,
					'detail'			=>		$detail,
					'sujet'				=>		$sujet
						            
					);
				
					$CI->db->insert('mails_log', $data_mail);
	}












}

/* End of file OS_users.php */
/* Location: ./application/libraries/OS_users.php */
