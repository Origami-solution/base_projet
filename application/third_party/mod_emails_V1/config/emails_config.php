<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Module de gestion des emails
 * Gilles GUIGON - contact@origami-solution.com
 * 13 05 2015
 *
 * Utilisation $this->config->item("mod_email_url_template_default_header");
 */



$config['email_administrateur']				 = "contact@origami-solution.com";
$config['email_administrateur_type']	 	 = "mail"; // mail / smtp

$config['email_administrateur_host']		= "smtp.gmail.com";
$config['email_administrateur_user']		= "guigon.gilles@gmail.com";
$config['email_administrateur_pass']		= "nDa...";
$config['email_administrateur_port']		= 465;

/*
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.gmail.com';
$config['smtp_user'] = 'guigon.gilles@gmail.com';
$config['smtp_pass'] = 'nDa00..';
$config['smtp_port'] = 465;
*/

$config['email_administrateur_bcc']	= "winprod@sfr.fr"; //Tous les envois en doublons






/**
 * URL du template default Le header et le footer et CSS
 * 
 * mod_email_url_template_default_css       = URL du CSS
 * mod_email_url_template_default_header    = URL du header
 * mod_email_url_template_default_footer    = URL du footer
 * 
 * base_url("")."assets/templates/partipromo/css/template_email.css
 */


$config['mod_email_url_template_default_css'] =  theme()."/emails/template_email_css_default.tpl.php";

$config['mod_email_url_template_default_header'] =  theme()."/emails/template_email_header_default.tpl.php";

$config['mod_email_url_template_default_footer'] =  theme()."/emails/template_email_footer_default.tpl.php";




/**assets/templates/partipromo/images/logo.png
 * URL du logo
 * base_url("")."assets/templates/partipromo/images/logo_tn.jpg"
 * 
 */
$config['mod_email_url_logo']	=  base_url("")."assets/".theme()."/plugins/emails/images/logo.png";

$config['mod_email_url_image_footer']	=  base_url("")."assets/".theme()."/plugins/emails/images/footer-barre.jpg";
