<?php 


/**
 * Signature en bas des emails
 * %1\$s 	=	URL du site
 * %2\$s 	=	Email du site
 */

$lang['email_text_Signature'] = "

<p>
Site : %1\$s
<br>
Email : %2\$s
<br>
</p>


";


/**
 * <a class="" href="%1\$s">%1\$s</a>
 * <a class="" href="mailto:%2\$s"></a>
 */






/**
 * Message de l'email d'inscription au site
 * %1\$s  = Nom d'utilsateur
 * %2\$s  = Nom du site
 * %3\$s  = Identifiant (email)
 * %4\$s  = Mot de passe
 */


$lang['email_text_inscription'] = "
<p>
Bonjour %1\$s,
<br>
<br>
Nous vous confirmons la bonne inscription sur le site %2\$s.
<br>
Voici un rappel de vos identifiants :
<ul>
	<li>Login : <strong>%3\$s</strong></li>
	<li>Mot de passe : <strong>%4\$s</strong></li>
</ul>
<em>Attention, ces identifiants sont confidentiels</em>
<br>
Toute l'équipe vous remercie de votre inscription.
<br>
A bientôt sur %2\$s
<br>
</p><br><br>
";


$lang['email_sujet_inscription'] = "Inscription sur le site %1\$s";








/**
 * Message de l'email de récupératino du mot de passe
 * 
 * %1\$s  = Nom d'utilsateur
 * %2\$s  = Nom du site
 * %3\$s  = Identifiant (email)
 * %4\$s  = Mot de passe
 */


$lang['email_text_recup_mot_passe'] = "
<p>
Bonjour %1\$s,
<br>
<br>
Vous avez demandé la récupération de vos identifiants sur le site %2\$s.
<br>
Voici un rappel de vos identifiants :
<ul>
	<li>Login : <strong>%3\$s</strong></li>
	<li>Mot de passe : <strong>%4\$s</strong></li>
</ul>
<em>Attention, ces identifiants sont confidentiels</em>
<br>
Toute l'équipe vous remercie de votre inscription.
<br>
A bientôt sur %2\$s
<br>
</p><br><br>
";


$lang['email_sujet_recup_mot_passe'] = "Récupération du mot de passe du site %1\$s";









?>