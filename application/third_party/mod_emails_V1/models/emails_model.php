<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class emails_model extends CI_Model {


/**
 * Récup la liste des log mails
 * @date   2015-05-16
 * @auteur Gilles     Guigon        - contact@origami-solution.com
 * @return [type]     [description]
 */
function get() 
     {
       
         // $this->db->where('id',$id);
          $this->db->order_by('id',"DESC");
          $this->db->limit(5);

          $query = $this->db->get('mails_log');
          
         if($query->num_rows()>0)
             {
                 foreach($query->result() as $row)
            {
                 $data[] = $row;
      }
      return $data;
        }
}


	

}

/* End of file emails_model.php */
/* Location: ./application/models/emails_model.php */