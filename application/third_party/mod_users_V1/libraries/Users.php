<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 	GESTION DES UTILISATEURS DU SITE
 *  
 *
 *
 * @package		CodeIgniter 2.2.1
 * @author 		Guigon Gilles contact@origami-solution.com
 * @version     1.0.0
 *
 **/



class Users
{
  protected 	$ci;

	public function __construct()
	{

        $this->__Verif_table();
        $this->__verif();
	}

	

		


	public function __verif()
		{
				$CI =& get_instance(); 

				
				//$CI->config->load('emails_config');
				$CI->load->helper('users');
				
		}

 







public function __Verif_table()
	{
  		$CI =& get_instance(); 
		$CI->load->dbforge();


		if (!$CI->db->table_exists('users'))
		{
		  
			$fields = array(
					'id' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'null' => FALSE,'auto_increment' => TRUE),
					'civilite' => array('type' => 'ENUM("Mr","Mme")','default' => 'Mr','null' => FALSE),
					'nom' => array('type' => 'VARCHAR','constraint' => 55,'null' => TRUE,'default' => ''),
                    'prenom' => array('type' => 'VARCHAR','constraint' => 55,'null' => TRUE,'default' => ''),
                    'email' => array('type' => 'VARCHAR','constraint' => 55,'null' => FALSE,'default' => ''),
                    'pass' => array('type' => 'VARCHAR','constraint' => 255,'null' => FALSE,'default' => ''),
                    'pseudo' => array('type' => 'VARCHAR','constraint' => 55,'null' => TRUE,'default' => ''),
                    'statut' => array('type' => 'ENUM("Actif","Inactif","Bloquer","Supprimer")','null' => FALSE,'default' => 'Actif'),
                    'date_inscription' => array('type' => 'DATETIME','null' => FALSE),
                   	'compte_type' => array('type' => 'ENUM("Utilisateur","Modérateur","Administrateur","Gestionnaire","Comptabilité")','null' => FALSE,'default' => 'Utilisateur'),
                    'date_last_connexion' => array('type' => 'DATETIME','null' => TRUE),
                    'avatar' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
					'adresse' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'code_postal' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'ville' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'pays' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'tel_fixe' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'tel_portable' => array('type' => 'VARCHAR','constraint' => 255,'null' => TRUE,'default' => ''),
                    'date_de_naissance' => array('type' => 'DATE'),
                    'hash' => array('type' => 'VARCHAR','constraint' => 255,'null' => FALSE,'default' => ''),
                );

        $CI->dbforge->add_field($fields);
        $CI->dbforge->add_key('id', TRUE);
        $CI->dbforge->create_table('users', TRUE);

	
        /**
         * Création du compte administrateur
         */
        
		

				$encrypted_string = encrypt("test55");

				$data_user = array(
						            'civilite' => 'Mr',
						            'nom' => "GUIGON",
						            'prenom' => "Gilles",
						            'pseudo' => "Gilles GUIGON",
						            'pass' => $encrypted_string,
						            'email' => "contact@origami-solution.com",
						            'statut' => "Actif",
						            'compte_type' => "Administrateur",
						            'date_inscription' => date("Y-m-d H:i:s"),
						            'date_last_connexion' => date("Y-m-d H:i:s"),
						            'hash' => md5("contact@origami-solution.com"),
						            );
				
				$CI->db->insert('users', $data_user);


			/**
			 * Envoi du mail dans le log et a l'administrateur
			 */

				$CI->emails_lib->Mail__Inscription($data_user);

		}

	}


/*

	#	Récup les infos de l'utilisateur depuis son ID
	#
	#
	public function get($id_user)
	{


				$CI =& get_instance(); 

					$CI->db->where('id',$id_user);
					$query = $CI->db->get('os_users');
						if($query->num_rows()>0)
						{
						foreach($query->result() as $row)
								{
								$data[] = $row ;
								}

						return $data;
                		}
	


	}




	#	Ajoute l'utilisateur
	#	$email = Email a inscrire
	#	$compte = enum('Utilisateur', 'Modérateur', 'Administrateur ...
	public function Add_user($civilite,$nom,$prenom,$pseudo,$pass,$email,$compte,$statut)
	{
				$CI =& get_instance();

				$CI->load->helper('cryptage');

				$encrypted_string = encrypt($pass);

				$data_user = array(
						            'civilite' => $civilite,
						            'nom' => $nom,
						            'prenom' => $prenom,
						            'pseudo' => $pseudo,
						            'pass' => $encrypted_string,
						            'email' => $email,
						            'statut' => $statut,
						            'compte_type' => $compte,
						            'date_inscription' => date("Y-m-d H:i:s"),
						            'date_last_connexion' => date("Y-m-d H:i:s"),
						            'hash' => md5($email),
						            );
				
				$CI->db->insert('os_users', $data_user);
				$id_user = $CI->db->insert_id();

				$CI->OS_email->Envoi_mail_User($id_user,2);
	}
	

*/



}

/* End of file OS_users.php */
/* Location: ./application/libraries/OS_users.php */
