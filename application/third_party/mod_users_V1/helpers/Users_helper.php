<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');





/**
 * Retourne si l'utilisateur est connecté en admin ou non
 * Cela permet d'accèder a la partie administration
 */
if ( ! function_exists('verif_admin'))
{
  function verif_admin()
  { 
  $CI =& get_instance(); 


      $CI->load->model("users_model");
      $infos_users = $CI->users_model->get_info_compte_admin($CI->session->userdata('user_id'));

      if(!empty($infos_users)){

        if($infos_users == "Administrateur" OR $infos_users == "Comptabilité" OR $infos_users == "Gestionnaire"){

          return TRUE ;
        }else{

          return FALSE ;

        }
       
      }else{

        return FALSE ;
      }
  }


}



#Retourne l'image d'avatar si pas d'avatar ou fichier innéxistant, on met le fichier pas d'avatar
if ( ! function_exists('avatar_user'))
{
  function avatar_user($avatar, $taille = false)
  {  
      $CI =& get_instance();  
      

      
      if(!empty($taille)){


          $taille = "";
            

      }else{

          $taille = "small/";
      }


      if(!empty($avatar)){

        $avatar_exist = "assets/uploads/avatars/".$taille."".$avatar;

        #si le fichier existe
        if(file_exists($avatar_exist)){

            $avatar = base_url().$avatar_exist;
            

        }else{
        
        #aucun avatar on prend par defaut
        $avatar = base_url()."assets/uploads/avatars/".$taille."aucun.jpg";

        }




      }else{
        #aucun avatar on prend par defaut
        $avatar = base_url()."assets/uploads/avatars/".$taille."aucun.jpg";

      }


      return $avatar ;
  }
}




#retourne true si admin ou eta admin
# pour le menu de la page admin
# $liste_acces = array("Gestionnaire","Comptabilité")
if ( ! function_exists('typecompte'))
{
  function typecompte($liste_acces = false)
  { 
  $CI =& get_instance(); 

     $CI->load->model("users_model");
     $infos_users = $CI->users_model->get_info_compte_admin($CI->session->userdata('user_id'));
      if(!empty($infos_users)){

        if($infos_users == "Administrateur"){

          return TRUE ;
        }else{


          if(!empty($liste_acces)){

            if(in_array($infos_users, $liste_acces)){
              return TRUE ;
            }

          }else{
            return FALSE ;
          }


          //return FALSE ;

        }
       
      }else{

        return FALSE ;
      }
  }


}


